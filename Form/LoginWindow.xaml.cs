﻿using InventoryAdjustment.Models;
using InventoryAdjustment.Service;
using System;
using System.DirectoryServices;
using System.Windows;
using System.Windows.Input;

namespace InventoryAdjustment.Form
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
            txtUsername.Focus();
        }
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            getLogin();
        }

        void getLogin()
        {
            try
            {
                if (string.IsNullOrEmpty(txtUsername.Text) || string.IsNullOrEmpty(txtPassword.Password)) throw new Exception("Please enter username and password");

                if (Authenticate(txtUsername.Text, txtPassword.Password, Environment.UserDomainName))
                {
                    var dthelper = new DataHelper();
                    var userModel = dthelper.Get_User(txtUsername.Text);
                    if (userModel == null) throw new Exception("ชื่อผู้ใช้นี้ไม่ได้รับอนุญาติให้เข้าใช้งานระบบ");
                    GlobalVariable.User = userModel;
                    MenuWindow menuWindow = new MenuWindow("Menu");
                    menuWindow.Show();
                    Close();
                }
                else
                {
                    txtPassword.Password = string.Empty;
                    txtPassword.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private bool Authenticate(string userName, string password, string domainName)
        {
            try
            {
                DirectoryEntry entry = new DirectoryEntry("LDAP://" + domainName, userName, password);
                object nativeObject = entry.NativeObject;
                return true;
            }
            catch (DirectoryServicesCOMException e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtPassword.Password = string.Empty;
            txtUsername.Text = string.Empty;
        }

        private void LoginTextbox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) getLogin();
        }

        private void PasswordTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            txtPassword.SelectAll();
        }

        private void UsernameTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            txtUsername.SelectAll();
        }

        private void UsernameTextbox_LostFocus(object sender, RoutedEventArgs e)
        {
            var d = new DataHelper();
            if (!string.IsNullOrEmpty(txtUsername.Text)) txtUsername.Text = d.FirstCapital(txtUsername.Text);
        }
    }
}
