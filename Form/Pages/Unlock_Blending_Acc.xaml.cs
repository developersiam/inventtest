﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using InventoryAdjustment.Service;
using InventoryAdjustment.DAL;

namespace InventoryAdjustment.Form.Pages
{
    /// <summary>
    /// Interaction logic for Unlock_Blending2.xaml
    /// </summary>
    public partial class Unlock_Blending_Acc : Page
    {
        DataHelper dtHelper = new DataHelper();
        Utility utility = new Utility();
        sp_QC_SEL_BLENDINGHOUR_Result model = new sp_QC_SEL_BLENDINGHOUR_Result();
        int ApplicationID = 14;
        public Unlock_Blending_Acc()
        {
            InitializeComponent();
            txtPdno.Focus();
        }
        private void Search_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) getData();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            getData();
        }

        private void getData()
        {
            try
            {
                int Hour = 0;

                if (string.IsNullOrWhiteSpace(txtPdno.Text) || !int.TryParse(txtHour.Text.Trim(), out Hour))
                {
                    MessageBox.Show("กรุณาใส่ข้อมูลค้นหาให้ครบถ้วน", "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                model = dtHelper.Get_Blendingstatus(txtPdno.Text, Hour);
                if (model == null)
                {
                    MessageBox.Show("ไม่พบข้อมูล", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtPdno.Focus();
                    Clear();
                }
                else setData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            txtBodyPdno.Text = "";
            txtBodyCrop.Text = "";
            txtBodyPddate.Text = "";
            txtBodyhour.Text = "";
            txtBodyLocked.Text = "";
            btnUpdate.IsEnabled = false;
            imgLocked.Visibility = Visibility.Collapsed;
            tblWarningDesc.Visibility = Visibility.Collapsed;
        }

        private void setData()
        {
            txtBodyPdno.Text = model.pdno;
            txtBodyCrop.Text = model.crop == null ? "NULL" : model.crop.ToString();
            txtBodyPddate.Text = model.pddate.Value.ToString();
            txtBodyhour.Text = model.blendinghour.ToString();
            btnUpdate.IsEnabled = IsEnable();
        }

        private bool IsEnable()
        {
            //If Locked=0 its 'UnLocked' .. Return false (Not edit allowed)
            var backColor = new BrushConverter();
            var islocked = model.locked.GetValueOrDefault();
            var isblending = model.def.GetValueOrDefault(); // def = 1 = blending

            txtBodyLocked.Text = islocked ? "Locked" : "Unlocked";
            txtBodyLocked.Background = islocked ? (Brush)backColor.ConvertFrom("#FFF19393") : (Brush)backColor.ConvertFrom("#FFACF193");
            imgLocked.Visibility = islocked ? Visibility.Visible : Visibility.Collapsed;
            imgUnlocked.Visibility = islocked ? Visibility.Collapsed : Visibility.Visible;

            //If Def=1 its now 'Blending' .. Return false (Not edit allowed)
            tblWarningDesc.Text = isblending ? "*Production นี้กำลังอบอยู่" : "*";
            tblWarningDesc.Visibility = isblending ? Visibility.Visible : Visibility.Hidden;

            return !isblending && islocked;
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Update();
        }

        private void Update()
        {
            try
            {
                int Blendinghour = 0;

                if (string.IsNullOrWhiteSpace(txtBodyPdno.Text) || !int.TryParse(txtBodyhour.Text.Trim(), out Blendinghour))
                {
                    MessageBox.Show("ผิดพลาด กรุณาตรวจสอบข้อมูล");
                    return;
                }
                MessageBoxResult result = MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่?", "", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    if (dtHelper.Update_UnlockBlending(model, "Unlock_Blending_Acc"))
                    {
                        txtPdno.Text = txtBodyPdno.Text;
                        txtHour.Text = txtBodyhour.Text;
                        //SendEmail();
                        MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        getData();
                    }
                    else MessageBox.Show("ผิดพลาด!! ไม่สามรถบันทึกข้อมูลได้", "Error!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void SendEmail()
        {
            string subject = "Unlock Blendingstatus Pdno: " + model.pdno;
            string from = "it-info@siamtobacco.com";
            var mailTo = dtHelper.Get_MailTo(ApplicationID);
            string body = "\"Product Number\" : \"" + model.pdno + "\", \n" +
                          "\"Blending Hour\" : \"" + model.blendinghour + "\", \n" +
                          "\"Crop\" : \"" + (model.crop == null ? "NULL" : model.crop.ToString()) + "\", \n" +
                          "\"Product Date\" : \"" + model.pddate + "\", \n" +
                          "\"User locked\" : \"" + model.locked.GetValueOrDefault() + "\" " +
                          " ===> " +
                          "\"User locked\" : \"False\" \n" +
                          "\"Modified Date\" : \"" + DateTime.Now.ToString() + "\" \n" +
                          "\"Modified By : \"" + Models.GlobalVariable.User.Username + "\"";
            utility.SendEmail(subject, from, mailTo, body);
        }
    }
}
