﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using InventoryAdjustment.DAL;
using InventoryAdjustment.Service;

namespace InventoryAdjustment.Form.Pages
{
    /// <summary>
    /// Interaction logic for Reject_Packed.xaml
    /// </summary>
    public partial class Reject_Packed : Page
    {
        pd model = new pd();
        DataHelper dtHelper = new DataHelper();
        public Reject_Packed()
        {
            InitializeComponent();
            txtHeaderBarcode.Focus();
            //txtHeaderBarcode.Text = "SS15-0000747";
            //txtHeaderCrop.Text = "2015";
        }
        private void Search_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) getData();
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            getData();
        }
        private void Update_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) Update();
        }
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Update();
        }
        private void getData()
        {
            int Crop = 0;

            if (string.IsNullOrWhiteSpace(txtHeaderBarcode.Text) || !int.TryParse(txtHeaderCrop.Text.Trim(), out Crop))
            {
                MessageBox.Show("ผิดพลาด กรุณาตรวจสอบข้อมูล");
                return;
            }
            model = dtHelper.Get_PD(txtHeaderBarcode.Text, Crop);
            if (string.IsNullOrEmpty(model.bc))
            {
                MessageBox.Show("ไม่พบข้อมูล", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                txtHeaderBarcode.Focus();
                return;
            }
            txtBarcode.Text = model.bc;
            txtBodyBarcode.Text = model.bc;
            txtBodyCrop.Text = model.crop.ToString();
            txtBodyCaseno.Text = model.caseno.ToString();
            txtBodyFrompdno.Text = model.frompdno;
            txtBodyFrompdhour.Text = model.frompdhour.ToString();
            txtBodyCurrentgrade.Text = model.graders;
            txtBodyPackingdate.Text = model.packingdate.ToString();
            txtBodyPackingtime.Text = model.packingtime.ToString();
            txtBodyIssue.Text = model.issued.GetValueOrDefault() ? "ตัดออกจากระบบ" : "ยังคงอยู่ในระบบ";
            txtBodyIssueto.Text = model.issuedto;
            btnUpdate.IsEnabled = !model.issued.GetValueOrDefault();
            tblIssueDesc.Visibility = model.issued.GetValueOrDefault() ? Visibility.Visible : Visibility.Hidden;
        }
        private void Update()
        {
            if (string.IsNullOrWhiteSpace(txtBarcode.Text) || string.IsNullOrWhiteSpace(txtIssuedreason.Text) || string.IsNullOrWhiteSpace(txtIssuedTo.Text) || model.issued.GetValueOrDefault())
            {
                MessageBox.Show("ผิดพลาด กรุณาตรวจสอบข้อมูล");
                return;
            }

            MessageBoxResult result = MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่?", "", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                model.issuedto = txtIssuedTo.Text.Trim();
                model.issuedreason = txtIssuedreason.Text.Trim();

                if (dtHelper.Update_Reject_Pack(model))
                {
                    MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    getData();
                }
                else MessageBox.Show("ผิดพลาด!! ไม่สามรถบันทึกข้อมูลได้", "Error!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
