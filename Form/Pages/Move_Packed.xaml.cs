﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using InventoryAdjustment.DAL;
using InventoryAdjustment.Service;

namespace InventoryAdjustment.Form.Pages
{
    /// <summary>
    /// Interaction logic for Move_Packed.xaml
    /// </summary>
    public partial class Move_Packed : Page
    {
        pd model = new pd();
        DataHelper dtHelper = new DataHelper();
        public Move_Packed()
        {
            InitializeComponent();
            BarcodeTextbox.Focus();
            //txtHeaderBarcode.Text = "LN17-0037616";
            //txtHeaderCrop.Text = "2017";
        }
        private void Search_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) getData();
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            getData();
        }
        private void Update_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) Update();
        }
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Update();
        }
        private void getData()
        {
            int Crop = 0;

            if (string.IsNullOrWhiteSpace(BarcodeTextbox.Text) || !int.TryParse(CropTextbox.Text.Trim(), out Crop))
            {
                MessageBox.Show("ผิดพลาด กรุณาตรวจสอบข้อมูล");
                return;
            }
            model = dtHelper.Get_PD(BarcodeTextbox.Text, Crop);
            if (model == null)
            {
                MessageBox.Show("ไม่พบข้อมูล", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                BarcodeTextbox.Focus();
                return;
            }

            txtCaseno.Text = model.caseno.ToString();
            txtFromprgrade.Text = model.fromprgrade;
            txtGrader.Text = model.graders;
            txtGrade.Text = model.grade;
            txtCustomerrs.Text = model.customerrs;
            txtPackingdate.Text = model.packingdate.GetValueOrDefault().ToString("dd/MM/yyyy");
            txtPackingtime.Text = model.packingtime.GetValueOrDefault().ToString("HH:mm:ss");
            txtIssue.Text = model.issued.GetValueOrDefault() ? "ตัดออกจากระบบ" : "ยังคงอยู่ในระบบ";
            txtIssueto.Text = string.IsNullOrEmpty(model.issuedto) ? "-" : model.issuedto;
            
            var listPackingStatus = dtHelper.Get_PackingStatus_By_Date(model.packingdate);

            var listFrompdno = listPackingStatus.GroupBy(g => g.pdno).ToList();
            cboFrompdno.ItemsSource = listFrompdno;
            cboFrompdno.SelectedValue = model.frompdno;

            var listFrompdhour = listPackingStatus.Where(w => w.pdno == model.frompdno).OrderBy(o => o.packinghour).ToList();
            cboFrompdhour.ItemsSource = listFrompdhour;
            if (model.frompdhour.GetValueOrDefault() != 0) cboFrompdhour.SelectedValue = model.frompdhour;
            else cboFrompdhour.Text = "-";

            cboFrompdno.IsEnabled = !model.issued.GetValueOrDefault();
            cboFrompdhour.IsEnabled = !model.issued.GetValueOrDefault();
            btnUpdate.IsEnabled = !model.issued.GetValueOrDefault();
            tblIssueDesc.Visibility = model.issued.GetValueOrDefault() ? Visibility.Visible : Visibility.Hidden;
        }

        private void Update()
        {
            int Frompdhour = 0;

            if (string.IsNullOrWhiteSpace(BarcodeTextbox.Text) || 
                !int.TryParse(cboFrompdhour.Text, out Frompdhour) || 
                string.IsNullOrEmpty(cboFrompdno.Text) ||
                string.IsNullOrEmpty(txtFromprgrade.Text) ||
                model.issued.GetValueOrDefault())
            {
                MessageBox.Show("ไม่สามารถอัพเดทข้อมูลได้ กรุณาตรวจสอบ");
                return;
            }
            MessageBoxResult result = MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่?", "", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                model.frompdhour = Frompdhour;

                if (dtHelper.Update_Move_Pack(model))
                {
                    MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    getData();
                }
                else MessageBox.Show("ผิดพลาด!! ไม่สามรถบันทึกข้อมูลได้", "Error!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void cboFrompdno_DropDownClosed(object sender, EventArgs e)
        {
            cboFrompdhour.ItemsSource = null;
            var listPackingStatus = dtHelper.Get_PackingStatus_By_Date(model.packingdate);
            var listFrompdhour = listPackingStatus.Where(w => w.pdno == cboFrompdno.Text).OrderBy(o => o.packinghour).ToList();
            cboFrompdhour.ItemsSource = listFrompdhour;
            cboFrompdhour.SelectedValue = model.frompdhour;
            txtFromprgrade.Text = dtHelper.Get_PdSetupByPdno(cboFrompdno.Text).packedgrade;
        }
    }
}
