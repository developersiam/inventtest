﻿using System;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using InventoryAdjustment.Service;
using InventoryAdjustment.DAL;
using System.Collections.Generic;

namespace InventoryAdjustment.Form.Pages
{
    /// <summary>
    /// Interaction logic for Unlock_Green_Report.xaml
    /// </summary>
    public partial class Unlock_Green_Report : Page
    {
        StecDBMSEntities db = new StecDBMSEntities();
        DataHelper dtHelper = new DataHelper();
        public Unlock_Green_Report()
        {
            InitializeComponent();
            BindLeafLock();
            BindUserLock();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cboCrop.SelectedValue == null) { MessageBox.Show("กรุณาเลือก Crop"); return; }
                var reportList = db.sp_GreenLeaf_RPT_UNLOCK(cboCrop.Text).ToList();
                if (cboType.SelectedValue != null) { reportList = reportList.Where(w => w.type == cboType.Text).ToList(); }
                if (cboSubtype.SelectedValue != null) { reportList = reportList.Where(w => w.subtype == cboSubtype.Text).ToList(); }
                if (cboCompany.SelectedValue != null) { reportList = reportList.Where(w => w.company == cboCompany.Text).ToList(); }
                if (cboLeaflock.SelectedValue != null) { reportList = reportList.Where(w => w.leaflocked == (cboLeaflock.Text == "Locked")).ToList(); }
                if (cboUserlock.SelectedValue != null) { reportList = reportList.Where(w => w.userlocked == (cboUserlock.Text == "Locked")).ToList(); }

                if (reportList == null || !reportList.Any()) { MessageBox.Show("ไม่พบข้อมูล"); dtgReport.ItemsSource = null; return; }
                dtgReport.ItemsSource = reportList;
            }
            catch (Exception ex)
            {
                MessageBox.Show("ผิดพลาด ไม่สามารถเรียกข้อมูลได้ : " + ex.ToString());
            }
        }

        private void BindLeafLock()
        {
            if (cboLeaflock.Items.Count > 0) return;
            List<string> listLock = new List<string>();
            listLock.Add("Locked");
            listLock.Add("Unlocked");
            cboLeaflock.ItemsSource = listLock;
            cboLeaflock.SelectedIndex = 0;
        }

        private void BindUserLock()
        {
            if (cboUserlock.Items.Count > 0) return;
            List<string> listLock = new List<string>();
            listLock.Add("Locked");
            listLock.Add("Unlocked");
            cboUserlock.ItemsSource = listLock;
            cboUserlock.SelectedIndex = 0;
        }

        private void cboCrop_DropDownOpened(object sender, EventArgs e)
        {
            if (cboCrop.Items.Count <= 0 || cboCrop.Text.IndexOf("-- Select --") == 0)
                cboCrop.ItemsSource = new System.Windows.Forms.BindingSource(db.mats.Select(s => new { Crop = s.crop }).Distinct().ToList().OrderByDescending(o => o.Crop).ToList(), null);
        }

        private void cboType_DropDownOpened(object sender, EventArgs e)
        {
            if (cboType.Items.Count <= 0 || cboType.Text.IndexOf("-- Select --") == 0)
                cboType.ItemsSource = new System.Windows.Forms.BindingSource(db.types.Select(s => new { Type = s.type1 }).ToList(), null);
        }

        private void cboSubtype_DropDownOpened(object sender, EventArgs e)
        {
            if (cboSubtype.Items.Count <= 0 || cboSubtype.Text.IndexOf("-- Select --") == 0)
                cboSubtype.ItemsSource = new System.Windows.Forms.BindingSource(db.subtypes.Select(s => new { Subtype = s.subtype1 }).ToList(), null);
        }

        private void cboCompany_DropDownOpened(object sender, EventArgs e)
        {
            if (cboCompany.Items.Count <= 0 || cboCompany.Text.IndexOf("-- Select --") == 0)
                cboCompany.ItemsSource = new System.Windows.Forms.BindingSource(db.companies.Select(s => s.code).Select(int.Parse).Select(s => new { Company = s }).OrderBy(o => o.Company).ToList(), null);
        }
    }
}
