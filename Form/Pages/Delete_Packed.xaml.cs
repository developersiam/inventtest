﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using InventoryAdjustment.DAL;
using InventoryAdjustment.Service;

namespace InventoryAdjustment.Form.Pages
{
    /// <summary>
    /// Interaction logic for Delete_Packed.xaml
    /// </summary>
    public partial class Delete_Packed : Page
    {
        pd model = new pd();
        DataHelper dtHelper = new DataHelper();
        public Delete_Packed()
        {
            InitializeComponent();
            txtHeaderBarcode.Focus();
            //txtHeaderBarcode.Text = "SL15-0011235";
            //txtHeaderCrop.Text = "2015";
        }
        private void Search_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) getData();
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            getData();
        }
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Update();
        }
        private void getData()
        {
            try
            {
                int Crop = 0;
                if (string.IsNullOrWhiteSpace(txtHeaderBarcode.Text) || !int.TryParse(txtHeaderCrop.Text.Trim(), out Crop))
                {
                    MessageBox.Show("ผิดพลาด กรุณาตรวจสอบข้อมูล"); return;
                }
                model = dtHelper.Get_PD(txtHeaderBarcode.Text, Crop);
                if (model == null)
                {
                    MessageBox.Show("ไม่พบข้อมูล", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtHeaderBarcode.Focus(); return;
                }
                txtBarcode.Text = model.bc;
                txtCrop.Text = model.crop.ToString();
                txtBodyBarcode.Text = model.bc;
                txtBodyCrop.Text = model.crop.ToString();
                txtBodyCaseno.Text = model.caseno.ToString();
                txtBodyFrompdno.Text = string.IsNullOrEmpty(model.frompdno) ? " " : model.frompdno;
                txtBodyFrompdhour.Text = model.frompdhour.HasValue ? model.frompdhour.ToString() : " ";
                txtBodyCurrentgrade.Text = string.IsNullOrEmpty(model.graders) ? " " : model.graders;
                txtBodyPackingdate.Text = model.packingdate.HasValue ? model.packingdate.ToString() : " ";
                txtBodyPackingtime.Text = model.packingtime.HasValue ? model.packingtime.ToString() : " ";
                txtBodyIssue.Text = model.issued.Value ? "ตัดออกจากระบบแล้ว" : "ยังคงอยู่ในระบบ";
                txtBodyIssueto.Text = string.IsNullOrEmpty(model.issuedto) ? " " : model.issuedto;
                btnUpdate.IsEnabled = !model.issued.Value;
                tblIssueDesc.Visibility = model.issued.Value ? Visibility.Visible : Visibility.Hidden;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.ToString(), "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void Update()
        {
            int Crop;
            if (string.IsNullOrWhiteSpace(txtBarcode.Text) || !int.TryParse(txtCrop.Text.Trim(), out Crop))
            {
                MessageBox.Show("ผิดพลาด กรุณาตรวจสอบข้อมูล"); return;
            }

            MessageBoxResult result = MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่?", "", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                if (dtHelper.Update_Delete_Pack(model)) MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                else MessageBox.Show("ผิดพลาด!! ไม่สามรถบันทึกข้อมูลได้", "Error!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}