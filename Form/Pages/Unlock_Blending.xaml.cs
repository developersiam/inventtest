﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using InventoryAdjustment.Service;
using InventoryAdjustment.DAL;

namespace InventoryAdjustment.Form.Pages
{
    /// <summary>
    /// Interaction logic for Unlock_Blending.xaml
    /// </summary>
    public partial class Unlock_Blending : Page
    {
        DataHelper dtHelper = new DataHelper();
        Utility utility = new Utility();
        //Models.Blendingstatus model = new Models.Blendingstatus();
        sp_QC_SEL_BLENDINGHOUR_Result model = new sp_QC_SEL_BLENDINGHOUR_Result();
        int ApplicationID = 10;

        public Unlock_Blending()
        {
            InitializeComponent();
            txtPdno.Focus();
        }
        private void Search_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) getData();
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            getData();
        }
        private void getData()
        {
            try
            {
                int Hour = 0;

                if (string.IsNullOrWhiteSpace(txtPdno.Text) || !int.TryParse(txtHour.Text.Trim(), out Hour))
                {
                    MessageBox.Show("กรุณาใส่ข้อมูลค้นหาให้ครบถ้วน", "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                model = dtHelper.Get_Blendingstatus(txtPdno.Text, Hour);
                if (model == null)
                {
                    MessageBox.Show("ไม่พบข้อมูล", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtPdno.Focus();
                    Clear();
                }
                else setData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Update();
        }
        private void Clear()
        {
            txtBodyPdno.Text = "";
            txtBodyCrop.Text = "";
            txtBodyPddate.Text = "";
            txtBodyhour.Text = "";
            txtBodyLocked.Text = "";
            btnUpdate.IsEnabled = false;
            imgLocked.Visibility = Visibility.Collapsed;
            tblWarningDesc.Visibility = Visibility.Collapsed;
        }
        private void setData()
        {
            txtBodyPdno.Text = model.pdno;
            txtBodyCrop.Text = (model.crop == 0) || (model.crop == null) ? "-" : model.crop.ToString();
            txtBodyPddate.Text = model.pddate.ToString();
            txtBodyhour.Text = model.blendinghour.ToString();
            btnUpdate.IsEnabled = IsEnable();
        }
        private bool IsEnable()
        {
            //If Locked=0 its 'UnLocked' .. Return false (Not edit allowed)
            var backColor = new BrushConverter();
            var islocked = model.locked.GetValueOrDefault();
            var isblending = model.def.GetValueOrDefault(); // def = 1 = blending
            var istoday = model.pddate.Value.Date == DateTime.Today; //

            txtBodyLocked.Text = islocked ? "Locked" : "Unlocked";
            txtBodyLocked.Background = islocked ? (Brush)backColor.ConvertFrom("#FFF19393") : (Brush)backColor.ConvertFrom("#FFACF193");
            imgLocked.Visibility = islocked ? Visibility.Visible : Visibility.Collapsed;
            imgUnlocked.Visibility = islocked ? Visibility.Collapsed : Visibility.Visible;

            //If  now 'Blending' .. (Not edit allowed)
            if (isblending) tblWarningDesc.Text = "*Production นี้กำลังอบอยู่";
            //QC dept can only update within PDDate 
            //If PDDate is not 'Today' .. (Not edit allowed)
            else if (!istoday) tblWarningDesc.Text = "*วันที่ Production ไม่ตรงกับปัจจุบัน";

            tblWarningDesc.Visibility = isblending || !istoday ? Visibility.Visible : Visibility.Hidden;
            return islocked && !isblending && istoday;
        }
        private void Update()
        {
            try
            {
                int Blendinghour = 0;

                if (string.IsNullOrWhiteSpace(txtBodyPdno.Text) || !int.TryParse(txtBodyhour.Text.Trim(), out Blendinghour))
                {
                    MessageBox.Show("ไม่สามารถอัพเดทข้อมูลได้ กรุณาตรวจสอบข้อมูล");
                    return;
                }
                MessageBoxResult result = MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่?", "", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    if (dtHelper.Update_UnlockBlending(model, "Unlock_Blending"))
                    {
                        txtPdno.Text = txtBodyPdno.Text;
                        txtHour.Text = txtBodyhour.Text;
                        //SendEmail();
                        MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        getData();
                    }
                    else MessageBox.Show("ผิดพลาด!! ไม่สามรถบันทึกข้อมูลได้", "Error!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void SendEmail()
        {
            string subject = "Unlock Blendingstatus Pdno: " + model.pdno;
            string from = "it-info@siamtobacco.com";
            var mailTo = dtHelper.Get_MailTo(ApplicationID);
            string body = "\"Product Number\" : \"" + model.pdno + "\", \n" +
                          "\"Blending Hour\" : \"" + model.blendinghour + "\", \n" +
                          "\"Crop\" : \"" + (model.crop == 0 ? "NULL" : model.crop.ToString()) + "\", \n" +
                          "\"Product Date\" : \"" + model.pddate + "\", \n" +
                          "\"User locked\" : \"" + model.locked + "\" " +
                          " ===> " +
                          "\"User locked\" : \"False\" \n" +
                          "\"Modified Date\" : \"" + DateTime.Now.ToString() + "\" \n" +
                          "\"Modified By : \"" + Models.GlobalVariable.User.Username + "\"";
            utility.SendEmail(subject, from, mailTo, body);
        }
    }
}
