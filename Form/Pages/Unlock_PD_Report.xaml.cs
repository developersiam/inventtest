﻿using System;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using InventoryAdjustment.Service;
using InventoryAdjustment.DAL;

namespace InventoryAdjustment.Form.Pages
{
    /// <summary>
    /// Interaction logic for Unlock_PD_Report.xaml
    /// </summary>
    public partial class Unlock_PD_Report : Page
    {
        StecDBMSEntities db = new StecDBMSEntities();
        DataHelper dtHelper = new DataHelper();
        public Unlock_PD_Report()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cboCrop.SelectedValue == null) { MessageBox.Show("กรุณาเลือก Crop"); return; }
                var reportList = db.sp_PD_RPT_UNLOCK(cboCrop.Text).ToList();
                if (reportList == null || !reportList.Any()) { MessageBox.Show("ไม่พบข้อมูล"); dtgReport.ItemsSource = null; return; }
                dtgReport.ItemsSource = reportList;
            }
            catch (Exception ex)
            {
                MessageBox.Show("ผิดพลาด ไม่สามารถเรียกข้อมูลได้ : " + ex.ToString());
            }
        }

        private void cboCrop_DropDownOpened(object sender, EventArgs e)
        {
            if (cboCrop.Items.Count <= 0 || cboCrop.Text.IndexOf("-- Select --") == 0)
                cboCrop.ItemsSource = new System.Windows.Forms.BindingSource(db.pdsetups
                                                                                .GroupBy(grp => grp.crop)
                                                                                .Select(s => new { Crop = s.Key })
                                                                                .OrderByDescending(o => o.Crop).ToList(), null);
        }
    }
}
