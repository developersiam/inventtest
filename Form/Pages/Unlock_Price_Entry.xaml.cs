﻿using InventoryAdjustment.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InventoryAdjustment.Form.Pages
{
    /// <summary>
    /// Interaction logic for Unlock_Price_Entry.xaml
    /// </summary>
    public partial class Unlock_Price_Entry : Page
    {
        DataHelper dt;
        public Unlock_Price_Entry()
        {
            InitializeComponent();
            dt = new DataHelper();
            DocNoTextbox.Focus();
        }

        private void btnUnlock_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(DocNoTextbox.Text)) return;
            MessageBoxResult confirmation = MessageBox.Show("ต้องการปลดล็อก Document No. นี้หรือไม่?", "Confirmation", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
            if (confirmation != MessageBoxResult.Yes) return;
            if (dt.Unlock_Price_Entry(DocNoTextbox.Text)) MessageBox.Show("อัพเดทสำเร็จ", "Information", MessageBoxButton.OK);
            else MessageBox.Show("ผิดพลาด, ไม่สารมารถอัพเดทได้", "Error!", MessageBoxButton.OK);
            ReloadData();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            ReloadData();
        }

        private void DocNoTextbox_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter) ReloadData();
        }

        private void ReloadData()
        {
            MatDataGrid.ItemsSource = null;
            string _docNo = DocNoTextbox.Text.Trim();
            if (string.IsNullOrEmpty(_docNo)) return;
            // Get List Of Mat Data
            var matList = dt.GetList_MatByDocno(_docNo);
            if (matList.Count < 1) { MessageBox.Show("ไม่พบ Document No. " + _docNo, "Information"); return; }
            DocNoTextbox.IsEnabled = false;
            // Get MatRC Data
            string _rcNo = matList.FirstOrDefault().rcno;
            var rcModel = dt.Get_MatRCByRcno(_rcNo);
            if (rcModel == null) { MessageBox.Show("ไม่พบ Receive No. " + _rcNo, "Information"); return; }

            ReceiveNoTextbox.Text = _rcNo;
            ReceiveFromTextbox.Text = rcModel.rcfrom;
            BuyerTextbox.Text = rcModel.buyer;
            ClassifierTextbox.Text = rcModel.classifier;
            TotalBaleTextbox.Text = matList.Count().ToString("N0");
            RecordDateTextbox.Text = rcModel.dtrecord.ToString();
            PlaceTextbox.Text = rcModel.place;
            TruckNoTextbox.Text = rcModel.truckno;
            decimal sumWeightBuy = matList.Sum(s => s.weightbuy).GetValueOrDefault();
            TotalWeightTextbox.Text = sumWeightBuy.ToString("N2");
            double sumPrice = matList.Sum(s => s.price).GetValueOrDefault();
            TotalPriceTextbox.Text = sumPrice.ToString("N2");
            double avgPrice = sumPrice / Convert.ToDouble(matList.Count());
            AvgPriceTextbox.Text = avgPrice.ToString("N2");
            bool isLock = matList.FirstOrDefault().leaflocked.GetValueOrDefault();
            UnlockButton.IsEnabled = isLock; //if it locked button will enable
            LockStackPanel.Visibility = isLock ? Visibility.Visible : Visibility.Collapsed;
            UnlockStackPanel.Visibility = isLock ? Visibility.Collapsed : Visibility.Visible;

            MatDataGrid.ItemsSource = matList.OrderBy(o => o.baleno).ToList();
        }

        private void ClearForm()
        {
            DocNoTextbox.Text = string.Empty;
            DocNoTextbox.IsEnabled = true;
            MatDataGrid.ItemsSource = null;
            ReceiveNoTextbox.Text = string.Empty;
            ReceiveFromTextbox.Text = string.Empty;
            BuyerTextbox.Text = string.Empty;
            ClassifierTextbox.Text = string.Empty;
            TotalBaleTextbox.Text = string.Empty;
            RecordDateTextbox.Text = string.Empty;
            PlaceTextbox.Text = string.Empty;
            TruckNoTextbox.Text = string.Empty;
            TotalWeightTextbox.Text = string.Empty;
            TotalPriceTextbox.Text = string.Empty;
            AvgPriceTextbox.Text = string.Empty;
            UnlockButton.IsEnabled = false;
            LockStackPanel.Visibility = Visibility.Collapsed;
            UnlockStackPanel.Visibility = Visibility.Visible;
            DocNoTextbox.Focus();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }
    }
}
