﻿using InventoryAdjustment.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InventoryAdjustment.Form.Pages
{
    /// <summary>
    /// Interaction logic for NPI_Farmer.xaml
    /// </summary>
    public partial class NPI_Farmer : Page
    {
        DataHelper dtHelper = new DataHelper();
        List<DAL.NPI_Farmer> npi_farmerlist = new List<DAL.NPI_Farmer>();
        public NPI_Farmer()
        {
            InitializeComponent();
            ReloadDataGrid();
        }

        private void FarmerFilterTextbox_KeyUp(object sender, KeyEventArgs e)
        {
            ReloadDataGrid();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDataGrid();
        }

        private void ReloadDataGrid()
        {
            try
            {
                NPIFarmerDataGrid.ItemsSource = null;

                string filterFarmerCode = SearchFarmerCodeTextbox.Text.Trim();
                string filterName = SearchNameTextbox.Text.Trim();
                string filterLName = SearchLastNameTextbox.Text.Trim();
                if(!npi_farmerlist.Any() || npi_farmerlist.Count < 1) npi_farmerlist = dtHelper.GetNPI_Farmer();
                var result = npi_farmerlist;
                if (!string.IsNullOrEmpty(filterFarmerCode)) result = result.Where(w => w.FarmerCode.ToUpper().Contains(filterFarmerCode.ToUpper())).ToList();
                if (!string.IsNullOrEmpty(filterName)) result = result.Where(w => w.FirstName.Contains(filterName)).ToList();
                if (!string.IsNullOrEmpty(filterLName)) result = result.Where(w => w.LastName.Contains(filterLName)).ToList();
                NPIFarmerDataGrid.ItemsSource = result;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Alert!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearData();
                var selected = (DAL.NPI_Farmer)NPIFarmerDataGrid.SelectedItem;
                if (selected == null) return;
                FarmerCodeTextbox.Text = selected.FarmerCode;
                PrefixTextbox.Text = selected.Prefix;
                NameTextbox.Text = selected.FirstName;
                LastNameTextbox.Text = selected.LastName;
                UpdateButton.IsEnabled = true;
                AddButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Alert!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string farmerCode = FarmerCodeTextbox.Text.Trim();
                string prefix = PrefixTextbox.Text.Trim();
                string name = NameTextbox.Text.Trim();
                string lastName = LastNameTextbox.Text.Trim();
                if(string.IsNullOrEmpty(farmerCode))
                {
                    MessageBox.Show("กรุณาใส่ Farmer Code", "Alert!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                dtHelper.NPI_AddFarmer(new DAL.NPI_Farmer
                {
                    FarmerCode = farmerCode,
                    Prefix = prefix,
                    FirstName = name,
                    LastName = lastName
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Alert!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string farmerCode = FarmerCodeTextbox.Text.Trim();
                string prefix = PrefixTextbox.Text.Trim();
                string name = NameTextbox.Text.Trim();
                string lastName = LastNameTextbox.Text.Trim();
                if (string.IsNullOrEmpty(farmerCode))
                {
                    MessageBox.Show("กรุณาใส่ Farmer Code", "Alert!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                dtHelper.NPI_AddFarmer(new DAL.NPI_Farmer
                {
                    FarmerCode = farmerCode,
                    Prefix = prefix,
                    FirstName = name,
                    LastName = lastName
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Alert!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearData();
            AddButton.IsEnabled = true;
            UpdateButton.IsEnabled = false;
        }

        private void ClearData()
        {
            FarmerCodeTextbox.Text = "";
            PrefixTextbox.Text = "";
            NameTextbox.Text = "";
            LastNameTextbox.Text = "";
        }
    }
}
