﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using InventoryAdjustment.DAL;
using InventoryAdjustment.Service;

namespace InventoryAdjustment.Form.Pages
{
    /// <summary>
    /// Interaction logic for Re_Packing.xaml
    /// </summary>
    public partial class Re_Packing : Page
    {
        pd model = new pd();
        DataHelper dtHelper = new DataHelper();
        public Re_Packing()
        {
            InitializeComponent();
            BarcodeTextbox.Focus();
        }
        private void Search_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) getData();
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            getData();
        }

        private void getData()
        {
            int Crop = 0;

            if (string.IsNullOrWhiteSpace(BarcodeTextbox.Text) || !int.TryParse(CropTextbox.Text.Trim(), out Crop))
            {
                MessageBox.Show("ผิดพลาด กรุณาตรวจสอบข้อมูล");
                return;
            }
            model = dtHelper.Get_PD(BarcodeTextbox.Text, Crop);
            if (model == null)
            {
                MessageBox.Show("ไม่พบข้อมูล", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                BarcodeTextbox.Focus();
                return;
            }

            txtCaseno.Text = model.caseno.ToString();
            txtToprgrade.Text = model.toprgrade;
            txtGrader.Text = model.graders;
            txtGrade.Text = model.grade;
            txtCustomerrs.Text = model.customerrs;
            txtPackingdate.Text = model.packingdate.GetValueOrDefault().ToString("dd/MM/yyyy");
            txtPackingtime.Text = model.packingtime.GetValueOrDefault().ToString("HH:mm:ss");
            txtIssue.Text = model.issued.GetValueOrDefault() ? "ตัดออกจากระบบ" : "ยังคงอยู่ในระบบ";
            txtIssueto.Text = string.IsNullOrEmpty(model.issuedto) ? "-" : model.issuedto;

            var listPackingStatus = dtHelper.Get_PackingStatus_By_Date(model.packingdate);

            var listTopdno = listPackingStatus.GroupBy(g => g.pdno).ToList();
            cboTopdno.ItemsSource = listTopdno;
            cboTopdno.SelectedValue = model.topdno;

            var listTopdhour = listPackingStatus.Where(w => w.pdno == model.frompdno).OrderBy(o => o.packinghour).ToList();
            cboTopdno.ItemsSource = listTopdhour;
            if (model.frompdhour.GetValueOrDefault() != 0) cboTopdno.SelectedValue = model.topdhour;
            else cboTopdno.Text = "-";

            cboTopdno.IsEnabled = !model.issued.GetValueOrDefault();
            cboTopdhour.IsEnabled = !model.issued.GetValueOrDefault();
            btnUpdate.IsEnabled = !model.issued.GetValueOrDefault();
            tblIssueDesc.Visibility = model.issued.GetValueOrDefault() ? Visibility.Visible : Visibility.Hidden;
        }

        private void cboTopdno_DropDownClosed(object sender, EventArgs e)
        {
            cboTopdhour.ItemsSource = null;
            var listPackingStatus = dtHelper.Get_PackingStatus_By_Date(model.packingdate);
            var listTopdhour = listPackingStatus.Where(w => w.pdno == cboTopdno.Text).OrderBy(o => o.packinghour).ToList();
            cboTopdhour.ItemsSource = listTopdhour;
            cboTopdhour.SelectedValue = model.topdhour;
            txtToprgrade.Text = dtHelper.Get_PdSetupByPdno(cboTopdno.Text).packedgrade;
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
