﻿using InventoryAdjustment.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InventoryAdjustment.Form.Pages
{
    /// <summary>
    /// Interaction logic for Unlock_Receiving.xaml
    /// </summary>
    public partial class Unlock_Receiving : Page
    {
        DataHelper dt;
        public Unlock_Receiving()
        {
            InitializeComponent();
            dt = new DataHelper();
            RcnoTextbox.Focus();
        }

        private void RcnoTextbox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) ReloadData();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            ReloadData();
        }

        private void ReloadData()
        {
            MatDataGrid.ItemsSource = null;
            // Get MatRC Data
            string _rcNo = RcnoTextbox.Text;
            if (string.IsNullOrEmpty(_rcNo)) return;
            var rcModel = dt.Get_MatRCByRcno(_rcNo);
            if (rcModel == null) { MessageBox.Show("ไม่พบ Receive No. " + _rcNo, "Information"); return; }
            RcnoTextbox.IsEnabled = false;
            var matList = dt.GetList_MatByRcNo(_rcNo);

            ReceiveFromTextbox.Text = rcModel.rcfrom;
            BuyerTextbox.Text = rcModel.buyer;
            ClassifierTextbox.Text = rcModel.classifier;
            TotalBaleTextbox.Text = matList.Count().ToString("N0");
            RecordDateTextbox.Text = rcModel.dtrecord.ToString();
            PlaceTextbox.Text = rcModel.place;
            TruckNoTextbox.Text = rcModel.truckno;
            decimal sumWeight = matList.Sum(s => s.weight).GetValueOrDefault();
            TotalWeightTextbox.Text = sumWeight.ToString("N2");
            decimal sumWeightBuy = matList.Sum(s => s.weightbuy).GetValueOrDefault();
            TotalWeightBuyTextbox.Text = sumWeightBuy.ToString("N2");
            CheckerNameTextbox.Text = rcModel.checker;
            CheckerApproveDateTextbox.Text = rcModel.checkerapproveddate.ToString();
            //CheckerApproveDateTextbox.Text = rcModel.checkerapproveddate.GetValueOrDefault().ToString("dd-MM-yyyy");
            //if checker is 'Locked' unlock button will enable
            bool isLock = rcModel.checkerlocked.GetValueOrDefault();
            UnlockButton.IsEnabled = isLock; 
            LockStackPanel.Visibility = isLock ? Visibility.Visible : Visibility.Collapsed;
            UnlockStackPanel.Visibility = isLock ? Visibility.Collapsed : Visibility.Visible;

            MatDataGrid.ItemsSource = matList.ToList();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void ClearForm()
        {
            RcnoTextbox.Text = string.Empty;
            RcnoTextbox.IsEnabled = true;
            MatDataGrid.ItemsSource = null;
            ReceiveFromTextbox.Text = string.Empty;
            BuyerTextbox.Text = string.Empty;
            ClassifierTextbox.Text = string.Empty;
            TotalBaleTextbox.Text = string.Empty;
            RecordDateTextbox.Text = string.Empty;
            PlaceTextbox.Text = string.Empty;
            TruckNoTextbox.Text = string.Empty;
            TotalWeightTextbox.Text = string.Empty;
            TotalWeightBuyTextbox.Text = string.Empty;
            UnlockButton.IsEnabled = false;
            LockStackPanel.Visibility = Visibility.Collapsed;
            UnlockStackPanel.Visibility = Visibility.Visible;
            RcnoTextbox.Focus();
        }

        private void UnlockButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(RcnoTextbox.Text)) return;
                if (MessageBox.Show("ต้องการปลดล็อก Receive No. นี้หรือไม่?", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    dt.Unlock_Receiving(RcnoTextbox.Text);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Information", MessageBoxButton.OK);
                    ReloadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
