﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using InventoryAdjustment.Service;
using InventoryAdjustment.DAL;

namespace InventoryAdjustment.Form.Pages
{
    /// <summary>
    /// Interaction logic for Unlock_Packing.xaml
    /// </summary>
    public partial class Unlock_Packing : Page
    {
        DataHelper dtHelper = new DataHelper();
        Utility utility = new Utility();
        sp_QC_SEL_PACKINGHOUR_Result model = new sp_QC_SEL_PACKINGHOUR_Result();
        int ApplicationID = 11;
        public Unlock_Packing()
        {
            InitializeComponent();
            txtHeaderPdno.Focus();
        }
        private void Search_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) getData();
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            getData();
        }
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Update();
        }
        private void getData()
        {
            try
            {
                int Hour = 0;
                if (string.IsNullOrWhiteSpace(txtHeaderPdno.Text) || !int.TryParse(txtHeaderHour.Text.Trim(), out Hour))
                {
                    MessageBox.Show("ผิดพลาด กรุณาตรวจสอบข้อมูล");
                    return;
                }
                model = dtHelper.Get_Packingstatus(txtHeaderPdno.Text, Hour);
                if (model == null)
                {
                    MessageBox.Show("ไม่พบข้อมูล", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtHeaderPdno.Focus();
                    Clear();
                }
                else setData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void Clear()
        {
            txtBodyPdno.Text = "";
            txtBodyCrop.Text = "";
            txtBodyPddate.Text = "";
            txtBodyhour.Text = "";
            txtBodyLocked.Text = "";
            btnUpdate.IsEnabled = false;
            imgLocked.Visibility = Visibility.Collapsed;
            tblWarningDesc.Visibility = Visibility.Collapsed;
        }
        private void setData()
        {
            txtBodyPdno.Text = model.pdno;
            txtBodyCrop.Text = model.crop == null ? "-" : model.crop.ToString();
            txtBodyPddate.Text = model.pddate.ToString();
            txtBodyhour.Text = model.packinghour.ToString();
            btnUpdate.IsEnabled = IsEnable();
        }
        private bool IsEnable()
        {
            //If Locked=0 its 'UnLocked' .. (Not edit allowed)
            var islocked = model.locked.GetValueOrDefault();
            var backColor = new BrushConverter();
            txtBodyLocked.Text = islocked ? "Locked" : "Unlocked";
            txtBodyLocked.Background = islocked ? (Brush)backColor.ConvertFrom("#FFF19393") : (Brush)backColor.ConvertFrom("#FFACF193");
            imgLocked.Visibility = islocked ? Visibility.Visible : Visibility.Collapsed;

            //If Def=1 its now 'Packing' .. (Not edit allowed)
            var ispacking = model.def.GetValueOrDefault();
            var istoday = model.pddate.GetValueOrDefault().Date == DateTime.Today;
            if (ispacking)
            {
                tblWarningDesc.Text = "*Production นี้กำลังอบอยู่ !!";
                tblWarningDesc.Visibility = Visibility.Visible;
            }
            //QC dept can only update within PDDate 
            //If PDDate is not 'Today' .. (Not edit allowed)
            else if (!istoday)
            {
                tblWarningDesc.Text = "*วันที่ Production ไม่ตรงกับปัจจุบัน !!";
                tblWarningDesc.Visibility = Visibility.Visible;
            }
            else
            {
                tblWarningDesc.Visibility = Visibility.Hidden;
            }
            return islocked && !ispacking && istoday; ;
        }
        private void Update()
        {
            try
            {
                if (model == null)
                {
                    MessageBox.Show("ไม่สามารถอัพเดทข้อมูลได้ กรุณาตรวจสอบข้อมูล");
                    return;
                }
                MessageBoxResult result = MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่?", "", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    if (dtHelper.Update_UnlockPacking(model, "Unlock_Packing"))
                    {
                        //SendEmail();
                        MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else MessageBox.Show("ผิดพลาด!! ไม่สามรถบันทึกข้อมูลได้", "Error!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void SendEmail()
        {
            string subject = "Delete Packingstatus Pdno: " + model.pdno;
            string from = "it-info@siamtobacco.com";
            var mailTo = dtHelper.Get_MailTo(ApplicationID);
            string body = "\"Product Number\" : \"" + model.pdno + "\", \n" +
                          "\"Packing Hour\" : \"" + model.packinghour + "\", \n" +
                          "\"Crop\" : \"" + (model.crop == null ? "NULL" : model.crop.ToString()) + "\", \n" +
                          "\"Product Date\" : \"" + model.pddate + "\", \n" +
                          "\"User locked\" : \"" + model.locked.GetValueOrDefault() + "\" \n" +
                          "\"Modified Date\" : \"" + DateTime.Now.ToString() + "\" \n" +
                          "\"Modified By : \"" + Models.GlobalVariable.User.Username + "\"";
            utility.SendEmail(subject, from, mailTo, body);
        }
    }
}
