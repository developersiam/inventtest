﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using InventoryAdjustment.DAL;
using InventoryAdjustment.Service;

namespace InventoryAdjustment.Form.Pages
{
    /// <summary>
    /// Interaction logic for Move_GreenIn.xaml
    /// </summary>
    public partial class Move_GreenIn : Page
    {
        StecDBMSEntities db = new StecDBMSEntities();
        DataHelper dtHelper = new DataHelper();
        mat model = new mat();
        public Move_GreenIn()
        {
            InitializeComponent();
            //txtHeaderBarcode.Text = "00000000000000201";
            //txtHeaderCrop.Text = "2015";
        }
        private void Search_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) getData();
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            getData();
        }
        private void Update_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) Update();
        }
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Update();
        }
        private void getData()
        {
            int Crop = 0;
            if (string.IsNullOrWhiteSpace(txtHeaderBarcode.Text) || !int.TryParse(txtHeaderCrop.Text.Trim(),out Crop))
            {
                MessageBox.Show("ผิดพลาด กรุณาตรวจสอบข้อมูล");
                return;
            }
            model = dtHelper.Get_Mat(txtHeaderBarcode.Text, Crop);
            if (model == null)
            {
                MessageBox.Show("ไม่พบข้อมูล", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                txtHeaderBarcode.Focus();
                return;
            }

            txtBodyRecieveno.Text = model.rcno;
            txtBodyCrop.Text = model.crop.ToString();
            txtBodyType.Text = model.type;
            txtBodyCompany.Text = model.company;
            txtBodyRcfrom.Text = model.rcfrom;
            txtBodyBaleno.Text = model.baleno.ToString();
            txtBodyClassify.Text = model.classify;
            txtBodyWeight.Text = model.weight.ToString();
            txtBodyIssueto.Text = model.issuedto;
            txtBodyTopdno.Text = model.topdno;
            txtBodyToHour.Text = model.topdhour.ToString();
            txtBarcode.Text = model.bc;
            
            var listPdno = db.pdsetups.Where(w => w.crop == Crop)
                                      .Select(s => new { Pdno = s.pdno }).Distinct().ToList()
                                      .OrderBy(o => o.Pdno).ToList();
            cboTopdno.ItemsSource = new System.Windows.Forms.BindingSource(listPdno,null);
            cboTopdno.SelectedIndex = listPdno.IndexOf(listPdno.SingleOrDefault(m => m.Pdno == model.topdno));
            cboTopdno.SelectedValue = model.topdno;

            //Can't using entity cause No primary key in packinghour.
            var listTopdHour = dtHelper.Get_PackingHour(model.topdno);
            cboTopdHour.ItemsSource = new System.Windows.Forms.BindingSource(listTopdHour, null);
            cboTopdHour.SelectedIndex = listTopdHour.FindIndex(member => member.packinghour == model.topdhour);
        }
        private void Update()
        {
            int Topdhour = 0;
            if (string.IsNullOrWhiteSpace(txtBarcode.Text) || string.IsNullOrWhiteSpace(cboTopdno.Text) || !int.TryParse(cboTopdHour.Text.Trim(), out Topdhour))
            {
                MessageBox.Show("ผิดพลาด กรุณาตรวจสอบข้อมูล");
                return;
            }
            MessageBoxResult result = MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่?", "", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                model.topdno = cboTopdno.Text;
                model.topdhour = Topdhour;
                model.toprgrade = db.pdsetups.SingleOrDefault(s => s.pdno == model.topdno).packedgrade;
                //UPDATE
                if (dtHelper.Update_Move_GreenIn(model))
                {
                    MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    getData();
                }
                else MessageBox.Show("ผิดพลาด!! ไม่สามรถบันทึกข้อมูลได้", "Error!", MessageBoxButton.OK, MessageBoxImage.Information);

            }
        }
        private void cboTopdno_DropDownClosed(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cboTopdno.Text)) return;
            var pd = db.pdsetups.FirstOrDefault(s => s.pdno == cboTopdno.Text);
            if (pd == null) MessageBox.Show("ไม่พบเลข Topdno ในระบบ", "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);

            //if (string.IsNullOrEmpty(db.pdsetups.SingleOrDefault(s => s.pdno == cboTopdno.Text).pdno)) return;
            cboTopdHour.ItemsSource = new System.Windows.Forms.BindingSource(dtHelper.Get_PackingHour(cboTopdno.Text), null);
            cboTopdHour.SelectedIndex = 0;
        }

        private void cboTopdno_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            if (string.IsNullOrEmpty(cboTopdno.Text)) return;
            var pd = db.pdsetups.FirstOrDefault(s => s.pdno == cboTopdno.Text);
            if (pd == null) MessageBox.Show("ไม่พบเลข Topdno ในระบบ", "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            //if (string.IsNullOrEmpty(db.pdsetups.SingleOrDefault(s => s.pdno == cboTopdno.Text).pdno)) return;
            cboTopdHour.ItemsSource = new System.Windows.Forms.BindingSource(dtHelper.Get_PackingHour(cboTopdno.Text), null);
            cboTopdHour.SelectedIndex = 0;
        }
    }
}
