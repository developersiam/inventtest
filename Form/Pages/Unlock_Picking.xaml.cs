﻿
using InventoryAdjustment.Service;
using InventoryAdjustment.ViewModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace InventoryAdjustment.Form.Pages
{
    /// <summary>
    /// Interaction logic for Unlock_Picking.xaml
    /// </summary>
    public partial class Unlock_Picking : Page
    {
        Unlock_Matrc_ViewModel model = new Unlock_Matrc_ViewModel();
        DataHelper dtHelper = new DataHelper();
        public Unlock_Picking()
        {
            InitializeComponent();
            //txtNumber.Text = "14-2138";
            txtNumber.Focus();
        }
        private void txtNumber_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) GetData();
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            GetData();
        }
        private void btnUnlock_Click(object sender, RoutedEventArgs e)
        {
            Update();
        }
        private void GetData()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(txtNumber.Text)) { MessageBox.Show("ผิดพลาด กรุณาตรวจสอบข้อมูล"); return; }
                model = dtHelper.GetMatRC("Picking", txtNumber.Text);
                if (model != null)
                {
                    //Set Control
                    BrushConverter backColor = new BrushConverter();
                    txtReceiveNumber.Text = model.rcno;
                    txtDate.Text = model.dtrecord.ToString();
                    txtTotalBale.Text = model.BALES.ToString();
                    txtTotalWeight.Text = model.WEIGHTS.GetValueOrDefault().ToString("N2");
                    bool userlock = model.userlocked.GetValueOrDefault();
                    txtUserLocked.Text = userlock ? "Locked" : "Unlocked";
                    txtUserLocked.Background = (Brush)backColor.ConvertFrom(userlock ? "#FFF19393" : "#FFACF193");
                    bool leaflock = model.leaflocked.GetValueOrDefault();
                    txtLeafLocked.Text = leaflock ? "Locked" : "Unlocked";
                    txtLeafLocked.Background = (Brush)backColor.ConvertFrom(leaflock ? "#FFF19393" : "#FFACF193");

                    imgLocked.Visibility = userlock || leaflock ? Visibility.Visible : Visibility.Hidden;
                    btnUnlock.IsEnabled = userlock || leaflock;
                }
                else MessageBox.Show("ไม่พบข้อมูล", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Update()
        {
            if (string.IsNullOrWhiteSpace(txtNumber.Text)) { MessageBox.Show("ผิดพลาด กรุณาตรวจสอบข้อมูล"); return; }
            MessageBoxResult result = MessageBox.Show("Do you want to unlock this document?", "", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                bool userlock = model.userlocked.GetValueOrDefault();
                bool leaflock = model.leaflocked.GetValueOrDefault();
                if (!(userlock || leaflock)) return;
                if (dtHelper.Update_LeafAcc_Unlock("Picking", model))
                {
                    MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "Information", MessageBoxButton.OK, MessageBoxImage.Information); GetData();
                }
                else
                {
                    MessageBox.Show("ผิดพลาด!! ไม่สามารถบันทึกข้อมูลได้ ", "Error!", MessageBoxButton.OK, MessageBoxImage.Information); return;
                }
            }
        }
    }
}