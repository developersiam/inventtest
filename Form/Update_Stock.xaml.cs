﻿using System;
using System.Windows;
using System.Collections.Generic;
using InventoryAdjustment.Models;
using InventoryAdjustment.Service;
using System.Linq;
using InventoryAdjustment.DAL;
using System.Xml.Serialization;
using Microsoft.Reporting.WinForms;

namespace InventoryAdjustment.Form
{
    /// <summary>
    /// Interaction logic for Update_Stock.xaml
    /// </summary>
    public partial class Update_Stock : Window
    {
        DataHelper dtHelper = new DataHelper();
        List<mat> listBarcode = new List<mat>();
        public Update_Stock()
        {
            InitializeComponent();
            lblUsername.Content = string.Format("[{0}]", GlobalVariable.User.Username);
            BindCropCombobox();
        }

        private void BindCropCombobox()
        {
            var result = new List<string>();
            var currentYear = DateTime.Now.Year;
            for (int i = 0; i <= 12; i++)
            {
                result.Add(currentYear.ToString("D4"));
                currentYear--;
            }
            cboCrop.ItemsSource = result;
            cboCrop.SelectedIndex = 0;
        }

        private void txtFileUpload_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            GetExcelFile();
        }

        private void btnExelSelect_Click(object sender, RoutedEventArgs e)
        {
            GetExcelFile();
        }

        private void dataGrid_LoadingRow(object sender, System.Windows.Controls.DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Update();
        }

        private void GetExcelFile()
        {
            try
            {
                if (cboCrop.SelectedValue == null) throw new Exception("กรุณาเลือก Crop.");
                Clear();

                var utility = new Utility();
                var fileDialog = new System.Windows.Forms.OpenFileDialog();
                fileDialog.Filter = "excel files (*.xlsx)|*.xlsx";
                fileDialog.FilterIndex = 2;
                fileDialog.RestoreDirectory = true;

                if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    txtFileUpload.Text = fileDialog.FileName;
                    var listBarcode_String = utility.ReadExcelFile(fileDialog); //Read Excel to list
                    listBarcode = utility.Match_ExcelData(listBarcode_String, cboCrop.SelectedValue.ToString()); //Match barcode

                    if (listBarcode != null || listBarcode.Count > 0)
                    {
                        //dataGrid.ItemsSource = listBarcode;
                        _ReportViewer.Reset();
                        var reportDataSource = new ReportDataSource();
                        reportDataSource.Value = listBarcode;
                        reportDataSource.Name = "DataSet1";
                        _ReportViewer.LocalReport.DataSources.Add(reportDataSource);
                        _ReportViewer.LocalReport.ReportEmbeddedResource = "InventoryAdjustment.Form.Reports.Rdlc.Rdlc001.rdlc";
                        ReportParameter[] parameters = new ReportParameter[1];
                        parameters[0] = new ReportParameter("lotNumber", cboCrop.Text, true);
                        _ReportViewer.LocalReport.SetParameters(parameters);
                        _ReportViewer.RefreshReport();
                        ShowErrorMessage();
                    }
                    else throw new Exception("กรุณาปิดไฟล์ Excel ที่กำลัง Upload."); //Must exit Uploading Excel file

                    txtTotalData.Text = listBarcode.Count.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ShowErrorMessage()
        {
            // Display Error Message With Issued And Mismatched Data
            int errorCount = 0;
            int issueCount = 0;
            string errorMessage = "";

            var mismatchedList = listBarcode.Where(w => w.RemarkChecked == "mismatched").ToList();
            var issueList = listBarcode.Where(w => w.issued.GetValueOrDefault()).ToList();
            
            if (mismatchedList != null && mismatchedList.Count > 0)
            {
                errorCount = mismatchedList.Count - 5;
                errorMessage += "Barcode ไม่ตรงกับในระบบ.. (Not found) \n";
                mismatchedList.Take(5).ToList().ForEach(errorBarcode => { errorMessage += "- " + errorBarcode.bc + " \n"; });
                if (errorCount > 0) errorMessage += "..และอีก " + errorCount + " หมายเลข. \n";
            }
            if (issueList != null && issueList.Count > 0)
            {
                issueCount = issueList.Count - 5;
                errorMessage += "Barcode ถูกตัดออกจากระบบ (Issued = 1) \n";
                issueList.Take(5).ToList().ForEach(issuseBarcode => { errorMessage += "- " + issuseBarcode.bc + " \n"; });
                if (issueCount > 0) errorMessage += "..และอีก " + issueCount + " หมายเลข. \n";
            }
            if (!string.IsNullOrEmpty(errorMessage.Trim())) MessageBox.Show(errorMessage,"ไม่สามารถบันทึกข้อมูลได้", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void Update()
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่?", "", MessageBoxButton.YesNoCancel, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (cboCrop.SelectedValue == null) throw new Exception("กรุณาเลือก Crop.");
                    if (cboIssueTo.SelectedValue == null) throw new Exception("กรุณาเลือก IssueTo.");
                    if (cboIssueReason.SelectedValue == null) throw new Exception("กรุณาเลือก IssueReason.");
                    if (listBarcode == null || listBarcode.Count <= 0) throw new Exception("ไม่พบข้อมูลที่ต้องการอัพเดท.");
                    var mismatchedList = listBarcode.Where(w => w.RemarkChecked == "mismatched").ToList();
                    if (mismatchedList != null && mismatchedList.Count > 0) throw new Exception("ไม่สามารถอัพเดทได้ ตรวจพบข้อมูลที่ไม่ตรงกับในระบบ.");
                    var issueList = listBarcode.Where(w => w.issued.GetValueOrDefault()).ToList();
                    if (issueList != null && issueList.Count > 0) throw new Exception("ไม่สามารถอัพเดทได้ ตรวจพบข้อมูลที่ถูกตัดจากระบบ.");
                    
                    int successData = 0, failedData = 0;
                    listBarcode.ForEach(Barcode =>
                    {
                        Barcode.issuedto = cboIssueTo.Text;
                        Barcode.issuedreason = cboIssueReason.Text;
                        if (dtHelper.Update_Mat_By_Excel(Barcode)) successData++;
                        else failedData++;
                    });
                    txtTotalSuccess.Text = successData.ToString();
                    txtTotalFailed.Text = failedData.ToString();
                    if(failedData == 0) dtHelper.Auditlog_Insert(Auditlog());
                    MessageBox.Show(string.Format("อัพเดทข้อมูล {0}/{1} รายการเรียบร้อยแล้ว !!", successData, listBarcode.Count), "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            txtFileUpload.Text = string.Empty;
            txtTotalData.Text = "0";
            txtTotalSuccess.Text = "0";
            txtTotalFailed.Text = "0";
            //dataGrid.ItemsSource = null;
            _ReportViewer.Reset();
            listBarcode.Clear();
        }

        private Audit_Log Auditlog()
        {
            var result = new Audit_Log
            {
                Corp = Convert.ToInt32(cboCrop.SelectedValue),
                TransTb = "Mat",
                TransDate = DateTime.Now,
                TransType = "Update",
                TransName = "Update_Stock",
                Remark = "Local Sale To TTM. ( " + listBarcode.Count + " barcodes)",
                AuditXml = SerializeAuditXml()
            };
            return result;
        }

        public string SerializeAuditXml()
        {
            string barcode = "";
            foreach (var item in listBarcode)
            {
                barcode += "<bc>" + item.bc + "</bc>";
            }
            return "<mat>" + barcode + "</mat>";
        }
    }
}
