﻿using System;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace InventoryAdjustment.Form.Reports
{
    /// <summary>
    /// Interaction logic for Unlock_Green_Report.xaml
    /// </summary>
    public partial class Unlock_Green_Report : Page
    {
        StecDBMSEntities db = new StecDBMSEntities();
        public Unlock_Green_Report()
        {
            InitializeComponent();
        }
        private void dtgReport_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                DataRow dr;
                dt.Columns.Add("Rcno");
                dt.Columns.Add("Bales");
                dt.Columns.Add("Leaflocked");
                dt.Columns.Add("Leafuser");
                dt.Columns.Add("Userlocked");
                dt.Columns.Add("User");
                dt.Columns.Add("Hsno");
                dt.Columns.Add("Tfno");
                dt.Columns.Add("Rgno");
                foreach (var item in db.sp_GreenLeaf_RPT_UNLOCK(cboCompany.Text, cboCrop.Text, cboType.Text, cboSubtype.Text).ToList())
                {
                    dr = dt.NewRow();
                    dr[0] = item.rcno;
                    dr[1] = item.BALES;
                    dr[2] = item.leaflocked;
                    dr[3] = item.leafuser;
                    dr[4] = item.userlocked;
                    dr[5] = item.user;
                    dr[6] = item.hsno;
                    dr[7] = item.tfno;
                    dr[8] = item.rgno;
                    dt.Rows.Add(dr);
                }
                dtgReport.ItemsSource = dt.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show("ผิดพลาด ไม่สามารถเรียกข้อมูลได้ : " + ex.ToString());
            }
        }
        private void cboCrop_DropDownOpened(object sender, EventArgs e)
        {
            if (cboCrop.Items.Count <= 0 || cboCrop.Text.IndexOf("-- Select --") == 0)
                cboCrop.ItemsSource = new System.Windows.Forms.BindingSource(db.mats
                                                                           .Select(s => new { Crop = s.crop }).Distinct().ToList()
                                                                           .OrderByDescending(o => o.Crop).ToList(), null);
        }
        private void cboType_DropDownOpened(object sender, EventArgs e)
        {
            if (cboType.Items.Count <= 0 || cboType.Text.IndexOf("-- Select --") == 0)
                cboType.ItemsSource = new System.Windows.Forms.BindingSource(db.types
                                                                  .Select(s => new { Type = s.type1 })
                                                                  .ToList(), null);
        }
        private void cboSubtype_DropDownOpened(object sender, EventArgs e)
        {
            if (cboSubtype.Items.Count <= 0 || cboSubtype.Text.IndexOf("-- Select --") == 0)
                cboSubtype.ItemsSource = new System.Windows.Forms.BindingSource(db.subtypes
                                                                  .Select(s => new { Subtype = s.subtype1 })
                                                                  .ToList(), null);
        }
        private void cboCompany_DropDownOpened(object sender, EventArgs e)
        {
            if (cboCompany.Items.Count <= 0 || cboCompany.Text.IndexOf("-- Select --") == 0)
                cboCompany.ItemsSource = new System.Windows.Forms.BindingSource(db.companies
                                                                  .Select(s => s.code)
                                                                  .Select(int.Parse)
                                                                  .Select(s => new { Company = s })
                                                                  .OrderBy(o => o.Company).ToList(), null);
        }
    }
}
