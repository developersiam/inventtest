﻿using InventoryAdjustment.Models;
using System;
using System.Windows;
using InventoryAdjustment.Service;

namespace InventoryAdjustment.Form
{
    /// <summary>
    /// Interaction logic for UnlockGreenLeafWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ContentSetting();
        }
        private void ContentSetting()
        {
            Utility utility = new Utility();
            string Username = GlobalVariable.User.Username;
            string ApplicationType = GlobalVariable.Application.ApplicationName;
            string ApplicationDescription = GlobalVariable.Application.Description;

            lblUsername.Content = string.Format("[{0}]", Username);
            lblHeader.Content = ApplicationType.Replace("_", " ");
            lblDescription.Content = ApplicationDescription;
            frame.Navigate(new Uri(utility.PageUri(ApplicationType), UriKind.RelativeOrAbsolute));
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
    }
}
