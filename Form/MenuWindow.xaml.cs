﻿using InventoryAdjustment.Models;
using InventoryAdjustment.Service;
using System;
using System.Windows;
using System.Windows.Controls;

namespace InventoryAdjustment.Form
{
    /// <summary>
    /// Interaction logic for MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow : Window
    {
        public MenuWindow(string menuType)
        {
            InitializeComponent();
            lblUsername.Content = string.Format("[{0}]", GlobalVariable.User.Username);
            btnLogout.Visibility = menuType == "Menu" ? Visibility.Visible : Visibility.Hidden;
            lblHeader.Content = menuType;
            GenerateButton(menuType);
        }
        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you want to logout?", "", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                LoginWindow loginWindow = new LoginWindow();
                loginWindow.Show();
                Close();
            }
        }
        public void GenerateButton(string Type)
        {
            DataHelper dtHelper = new DataHelper();
            //Add button as Grid's child
            int grdColumn = 0;
            int grdRow = 0;
            foreach (var item in dtHelper.Get_Menu(Type))
            {
                if (!string.IsNullOrEmpty(item.ApplicationName))
                {
                    if (grdColumn == 5) { grdColumn = 0; grdRow++; }
                    ImageButton imb = new ImageButton(item);
                    grdMain.Children.Add(imb);
                    Grid.SetColumn(imb, grdColumn);
                    Grid.SetRow(imb, grdRow);

                    grdColumn++;
                }
            }

            //Add button as StackPanel's child
            //int btnCount = 1;
            //StackPanel stp = new StackPanel();
            //stp.Orientation = Orientation.Horizontal;
            //stp.HorizontalAlignment = HorizontalAlignment.Center;
            //foreach (Models.Application item in dtHelper.Get_Menu())
            //{
            //    ImageButton imb = new ImageButton(item);
            //    Thickness margin = imb.Margin;
            //    margin.Left = margin.Right = 30;
            //    margin.Bottom = margin.Top = 15;
            //    imb.Margin = margin;
            //    stp.Children.Add(imb);
            //    if (btnCount != 1 && (5 % btnCount) == 0)
            //    {
            //        stpMain.Children.Add(stp);
            //        stp = new StackPanel();
            //        stp.Orientation = Orientation.Horizontal;
            //        stp.HorizontalAlignment = HorizontalAlignment.Center;
            //        btnCount = 0;
            //    }
            //    btnCount++;
            //}
        }
    }
}
