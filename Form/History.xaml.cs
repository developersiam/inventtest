﻿using InventoryAdjustment.Models;
using InventoryAdjustment.Service;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace InventoryAdjustment.Form
{
    /// <summary>
    /// Interaction logic for History.xaml
    /// </summary>
    public partial class History : Window
    {
        DataHelper dtHelper = new DataHelper();
        public History()
        {
            InitializeComponent();
            lblUsername.Content = string.Format("[{0}]", GlobalVariable.User.Username);

            //Get All Application (Except 'History') to Combobox
            cboType.ItemsSource = new System.Windows.Forms.BindingSource(dtHelper.Get_Menu("Menu").Where(p => (p.ApplicationID != 13) &&
                                                                                                              (p.ApplicationID != 14) &&
                                                                                                              (p.ApplicationID != 15) &&
                                                                                                              (p.ApplicationID != 16) &&
                                                                                                              (p.ApplicationID != 18) &&
                                                                                                              (p.ApplicationID != 19)), null);
            cboCrop.ItemsSource = dtHelper.Get_AuditLog_Crop();
        }
        private void dtgHistory_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }
        private void cboType_DropDownClosed(object sender, System.EventArgs e)
        {
            //Bind 'Crop' To Combobox
            //cboCrop.ItemsSource = new System.Windows.Forms.BindingSource(dtHelper.Get_AuditLog(cboType.Text), null);
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            int Crop;
            DateTime startDate1, endDate1;
            DateTime? startDate = null;
            DateTime? endDate = null;
            if (!int.TryParse(cboCrop.Text, out Crop)) return;
            if (!string.IsNullOrEmpty(dteStart.Text) || !string.IsNullOrEmpty(dteEnd.Text)) 
            {
                if (!DateTime.TryParse(dteStart.Text, out startDate1)) return;
                startDate = startDate1;
                if (!DateTime.TryParse(dteEnd.Text, out endDate1)) return;
                endDate = endDate1;
            }
            string TransName = cboType.Text;
            
            dtgHistory.ItemsSource = dtHelper.Get_AuditLog(TransName, Crop, startDate, endDate);
        }
    }
}
