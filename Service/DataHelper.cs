﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Configuration;
using System.Data;
using InventoryAdjustment.DAL;
//using InventoryAdjustment.Models;
using InventoryAdjustment.ViewModel;

namespace InventoryAdjustment.Service
{
    public class DataHelper
    {
        SqlConnection conStecDBMS = new SqlConnection(ConfigurationManager.ConnectionStrings["StecDBMS"].ConnectionString);
        //SqlConnection conStec = new SqlConnection(ConfigurationManager.ConnectionStrings["Stec"].ConnectionString);
        string Username;
        int UserID;
        public DataHelper()
        {
            try
            {
                Username = Models.GlobalVariable.User.Username;
                UserID = Models.GlobalVariable.User.UserID;
            }
            catch (Exception) {}
        }

        public string FirstCapital(string inputString)
        {
            inputString = inputString.ToLower();
            if (string.IsNullOrEmpty(inputString)) return string.Empty;
            // convert to char array of the string
            char[] letters = inputString.ToCharArray();
            // upper case the first char
            letters[0] = char.ToUpper(letters[0]);
            // return the array made of the new char array
            return new string(letters);
        }

        public Unlock_Matrc_ViewModel GetMatRC(string strPrarameter, string value)
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                    matrc rc = new matrc();
                    if (strPrarameter == "Regrade") rc = stecDbms.matrcs.SingleOrDefault(w => w.rgno == value);
                    else if (strPrarameter == "Handstrip") rc = stecDbms.matrcs.SingleOrDefault(w => w.hsno == value);
                    else if (strPrarameter == "Transfer") rc = stecDbms.matrcs.SingleOrDefault(w => w.tfno == value);
                    else if (strPrarameter == "Picking") rc = stecDbms.matrcs.SingleOrDefault(w => w.rcno == value);
                    if (rc == null) return null;
                    var mat = stecDbms.mats.Where(w => w.rcno == rc.rcno).ToList();
                    //if (mat == null) return null;

                    var x = new Unlock_Matrc_ViewModel
                    {
                        tfno = rc.tfno,
                        rgno = rc.rgno,
                        hsno = rc.hsno,
                        rcno = rc.rcno,
                        dtrecord = rc.dtrecord,
                        userlocked = rc.userlocked,
                        leaflocked = rc.leaflocked,
                        BALES = mat.Count(),
                        WEIGHTS = mat.Sum(s => s.weight),
                        crop = rc.crop
                    };

                    return new Unlock_Matrc_ViewModel
                    {
                        tfno = rc.tfno,
                        rgno = rc.rgno,
                        hsno = rc.hsno,
                        rcno = rc.rcno,
                        dtrecord = rc.dtrecord,
                        userlocked = rc.userlocked,
                        leaflocked = rc.leaflocked,
                        BALES = mat.Count(),
                        WEIGHTS = mat.Sum(s => s.weight),
                        crop = rc.crop
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public mat Get_Mat(string BC, int CROP)
        {
            using (StecDBMSEntities stecDbms = new StecDBMSEntities())
            {
                return stecDbms.mats.SingleOrDefault(m => m.bc == BC && m.crop == CROP);
            }
        }

        public pd Get_PD(string BC, int CROP)
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                    return stecDbms.pds.SingleOrDefault(s => s.bc == BC && s.crop == CROP);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public sp_QC_SEL_BLENDINGHOUR_Result Get_Blendingstatus(string Pdno, int Hour)
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                    var result = stecDbms.sp_QC_SEL_BLENDINGHOUR(Pdno, Hour).SingleOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public sp_QC_SEL_PACKINGHOUR_Result Get_Packingstatus(string Pdno, int Hour)
        {
            using (StecDBMSEntities stecDbms = new StecDBMSEntities())
            {
                var x = stecDbms.sp_QC_SEL_PACKINGHOUR(Pdno, Hour).SingleOrDefault();
                return x;
            }
        }

        public List<sp_GreenLeaf_Get_PackingStatus_Result> Get_PackingHour(string Pdno)
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                    return stecDbms.sp_GreenLeaf_Get_PackingStatus(Pdno).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_GreenLeaf_Sel_PackingStatus_By_Date_Result> Get_PackingStatus_By_Date(DateTime? Pddate)
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                    return stecDbms.sp_GreenLeaf_Sel_PackingStatus_By_Date(Pddate).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Application> Get_Menu(string Type)
        {
            try
            {
                using (STECEntities stec = new STECEntities())
                {
                    List<Application> result = new List<Application>();
                    Application report = new Application();
                    Application history = new Application();
                    var roleID = stec.UserRoles.SingleOrDefault(s => s.UserID == Models.GlobalVariable.User.UserID && s.Active == true).RoleID;
                    var menulist = stec.ApplicationRoles.Where(w => w.RoleID == roleID).ToList();
                    menulist.ForEach(f =>
                    {
                        var appDetail = stec.Applications.SingleOrDefault(s => s.ApplicationID == f.ApplicationID);
                        if (Type != "Menu" && ((f.ApplicationID == 16 || f.ApplicationID == 18))) result.Add(appDetail);
                        else if (Type == "Menu")
                        {
                            if (f.ApplicationID == 19) report = appDetail;
                            else if (f.ApplicationID == 13) history = appDetail;
                            else if (f.ApplicationID != 16 && f.ApplicationID != 18) result.Add(appDetail);
                        }
                    });

                    if (Type == "Menu") { result.Add(report); result.Add(history); }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Applications_MailTo> Get_MailTo(int ApplicationsID)
        {
            try
            {
                using (STECEntities stec = new STECEntities())
                {
                    return stec.Applications_MailTo.Where(w => w.ApplicationsID == ApplicationsID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public User Get_User(string Username)
        {
            using (STECEntities stec = new STECEntities())
            {
                return stec.Users.SingleOrDefault(s => s.Username == Username);
            }
        }

        public List<NPI_Farmer> GetNPI_Farmer()
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                    return stecDbms.NPI_Farmer.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<int> Get_AuditLog_Crop()
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                    List<int> result = new List<int>();
                    var listCrop = stecDbms.Audit_Log.Where(w => w.Corp != null)
                                                     .GroupBy(g => g.Corp)
                                                     .Select(s => new { Crop = s.Key })
                                                     .OrderByDescending(o => o.Crop).ToList();
                    listCrop.ForEach(f => result.Add(f.Crop.Value));
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Audit_Log> Get_AuditLog(string TransName, int Crop, DateTime? startDate, DateTime? endDate)
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                    var result = stecDbms.Audit_Log.Where(w => w.Corp == Crop).ToList();
                    if (!string.IsNullOrEmpty(TransName)) result.Where(w => w.TransName == TransName).ToList();
                    if (startDate != null && endDate != null) result.Where(w => w.TransDate >= startDate && w.TransDate <= endDate).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<mat> GetList_MatByDocno(string docNo)
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                    int _docNo = Convert.ToInt32(docNo);
                    return stecDbms.mats.Where(w => w.docno == _docNo).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public matrc Get_MatRCByRcno(string rcNo)
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                     return stecDbms.matrcs.SingleOrDefault(s => s.rcno == rcNo);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public pdsetup Get_PdSetupByPdno(string Pdno)
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                    return stecDbms.pdsetups.SingleOrDefault(s => s.pdno == Pdno);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_Receiving_SEL_MatByRcNo_Result> GetList_MatByRcNo(string rcNo)
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                    return stecDbms.sp_Receiving_SEL_MatByRcNo(rcNo).OrderBy(o => o.rcno)
                                                                    .ThenBy(t => t.docno)
                                                                    .ThenBy(t => t.baleno)
                                                                    .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update_LeafAcc_Unlock(string Type, Unlock_Matrc_ViewModel model)
        {
            try
            {
                using (StecDBMSEntities stectDbms = new StecDBMSEntities())
                {
                    switch (Type)
                    {
                        case "Regrade":
                            stectDbms.sp_GreenLeaf_UNLOCK_REGRADE(model.rgno, UserID);
                            break;
                        case "Handstrip":
                            stectDbms.sp_GreenLeaf_UNLOCK_HANDSTRIP(model.hsno, UserID);
                            break;
                        case "Transfer":
                            stectDbms.sp_GreenLeaf_UNLOCK_TRANSFER(model.tfno, UserID);
                            break;
                        case "Picking":
                            stectDbms.sp_GreenLeaf_UNLOCK_PICKING(model.rcno, UserID);
                            break;
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update_Move_GreenIn(mat model)
        {
            SqlCommand history, comm;
            string strcommand;
            conStecDBMS.Open();
            //History&Transaction
            strcommand = "DECLARE @t xml " +
                         "SET @t = (	SELECT	bc , " +
                         "					    (SELECT [rcno],[crop],[type],[subtype],[company],[rcfrom],[bc],[supplier] " +
                         "					            ,[supplier2],[baleno],[green],[classify],[mark],[weight],[weightbuy],[malcam] " +
                         "								,[hearson],[docno],[frombc],[fromclassify],[frommark],[fromcompany],[tobc],[toclassify] " +
                         "								,[tomark],[tocompany],[picking],[bz],[pallet],[pvpallet],[bay],[wh],[pvwh],[isno] " +
                         "								,[issued],[issuedto],[issueddate],[issuedtime],[issuedreason],[priceunit],[price],[priceno] " +
                         "								,[pvpriceunit],[pvprice],[pvpriceno],[firstprice],[adjpriceno],[adjdate],[adjtime],[adjtimes] " +
                         "								,[paidno],[paid],[rcstatus],[leafstatus],[locked],[dtrecord],[receiveduser],[issueduser] " +
                         "								,[adjuser],[qcuser],[oldbc],[tcharge],[leaflocked],[feedinguser],[blendingprice],[blendingpriceunit] " +
                         "								,[feedingdate],[feedingtime],[topddate],[toprgrade],[topdno],[topdhour],[frompddate],[fromprgrade] " +
                         "								,[frompdno],[frompdhour],[pkvalue],[price2],[price3],[gep%] as gep,[gepbaht],[redrycharge] " +
                         "								,[pdcost],[cropdef],[oldprice],[oldpriceunit],[bfno],[bftimes],[ntrm_no],[ntrm_nylon] " +
                         "								,[ntrm_plastic],[ntrm_rubber],[ntrm_string],[ntrm_metal],[ntrm_other],[feedingweight],[saleweight],[moisture_status] " +
                         "								,[rcdate],[wet],[CPAtestNo],[handle_charge],[price_wo_handle_charge],[priceunit_wo_handle_charge],[farmer_code],[id_card],[buying_date] " +
                         "								,[project_code],[topdremark],[frompdremark],[RemarkChecked],[RemarkCheckedDate],[RemarkCheckedUser],[BatchNo],[CPAResult] " +
                         "								,[CPAResultDate],[CPAResultUser],[Classifier],[ReplaceBales] " +
                         "					    FROM [StecDBMS].[dbo].[mat] E " +
                         "					    WHERE E.bc = m.bc " +
                         "  					FOR XML PATH ('details'),TYPE) " +
                         "			    FROM [StecDBMS].[dbo].[mat] m " +
                         "				WHERE bc = @bc " +
                         "				GROUP BY bc " +
                         "				FOR XML PATH ('oldLog'),type,ROOT('mat')) " +
                         "INSERT INTO Audit_Log " +
                         "VALUES (  @Crop, " +
                         "		    @TransTb, " +
                         "		    GETDATE(), " +
                         "  		@TransType, " +
                         "          @TransName, " +
                         "          (SELECT CONCAT(Topdno,',',Topdhour,',',Issued,',',toprgrade) FROM MAT WHERE bc = @bc), " +
                         "          @ChangeValue, " +
                         "          @Remark, " +
                         "          @UserID, " +
                         "          @t) ";
            history = new SqlCommand(strcommand, conStecDBMS);
            history.Parameters.AddWithValue("@bc", model.bc);
            history.Parameters.AddWithValue("@Crop", model.crop);
            history.Parameters.AddWithValue("@TransTb", "Mat");
            history.Parameters.AddWithValue("@TransType", "Update");
            history.Parameters.AddWithValue("@TransName", "Move_GreenIn");
            history.Parameters.AddWithValue("@ChangeValue", model.topdno + "," + model.topdhour + ",1," + model.toprgrade);
            history.Parameters.AddWithValue("@topdno", model.topdno);
            history.Parameters.AddWithValue("@Remark", "topdno,topdno,issued,toprgrade");
            history.Parameters.AddWithValue("@UserID", UserID);
            //update
            comm = new SqlCommand("sp_GREENLEAF_CHANGE_Hour_FROMGREENINPUT", conStecDBMS);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.AddWithValue("@topdhour", model.topdhour);
            comm.Parameters.AddWithValue("@topdno", model.topdno);
            comm.Parameters.AddWithValue("@bc", model.bc);
            comm.Parameters.AddWithValue("@toprgrade", model.toprgrade);

            return Update(conStecDBMS, history, comm);
        }

        public bool Update_Move_Pack(pd model)
        {
            SqlCommand history, comm;
            string strcommand;
            conStecDBMS.Open();
            //History & Transaction
            strcommand = "DECLARE @t xml " +
                         "SET @t = (	SELECT	bc , " +
                         "					(SELECT * " +
                         "					 FROM [StecDBMS].[dbo].[pd] E " +
                         "					 WHERE E.[bc] = m.bc " +
                         "					 FOR XML PATH ('details'),TYPE) " +
                         "		    	FROM [StecDBMS].[dbo].[pd] m " +
                         "  			WHERE bc = @bc " +
                         "	    		GROUP BY bc " +
                         "  			FOR XML PATH ('oldLog'),type,ROOT('pd')) " +
                         "INSERT INTO Audit_Log " +
                         "VALUES (  @Crop, " +
                         "		    @TransTb, " +
                         "		    GETDATE(), " +
                         "  		@TransType, " +
                         "          @TransName, " +
                         "          (SELECT FromPdHour FROM pd WHERE bc = @bc), " +
                         "          @ChangeValue, " +
                         "          @Remark, " +
                         "          @UserID, " +
                         "          @t) ";
            history = new SqlCommand(strcommand, conStecDBMS);
            history.Parameters.AddWithValue("@bc", model.bc);
            history.Parameters.AddWithValue("@Crop", model.crop);
            history.Parameters.AddWithValue("@TransTb", "Pd");
            history.Parameters.AddWithValue("@TransType", "Update");
            history.Parameters.AddWithValue("@TransName", "Move_Packed");
            history.Parameters.AddWithValue("@ChangeValue", model.frompdhour);
            history.Parameters.AddWithValue("@Remark", "FromPdHour");
            history.Parameters.AddWithValue("@UserID", UserID);
            //update
            comm = new SqlCommand("sp_QC_CHANGE_Hour_Production", conStecDBMS);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.AddWithValue("@bc", model.bc);
            comm.Parameters.AddWithValue("@frompdhour", model.frompdhour);

            return Update(conStecDBMS, history, comm);
        }

        public bool Update_Reject_Pack(pd model)
        {
            SqlCommand history, comm;
            string strcommand;
            conStecDBMS.Open();
            //History & Transaction
            strcommand = "DECLARE @t xml " +
                         "SET @t = (	SELECT	bc , " +
                         "					(SELECT * " +
                         "					 FROM [StecDBMS].[dbo].[pd] E " +
                         "					 WHERE E.[bc] = m.bc " +
                         "					 FOR XML PATH ('details'),TYPE) " +
                         "		    	FROM [StecDBMS].[dbo].[pd] m " +
                         "  			WHERE bc = @bc " +
                         "	    		GROUP BY bc " +
                         "  			FOR XML PATH ('oldLog'),type,ROOT('pd')) " +
                         "INSERT INTO Audit_Log " +
                         "VALUES (  @Crop, " +
                         "		    @TransTb, " +
                         "		    GETDATE(), " +
                         "  		@TransType, " +
                         "          @TransName, " +
                         "          (SELECT CONCAT(Issued, ',' ,Issuedto, ',' , CASE WHEN Issuedreason IS NULL THEN 'NULL' ELSE issuedreason END) FROM pd WHERE bc = @bc), " +
                         "          @ChangeValue, " +
                         "          @Remark, " +
                         "          @UserID, " +
                         "          @t) ";
            history = new SqlCommand(strcommand, conStecDBMS);
            history.Parameters.AddWithValue("@bc", model.bc);
            history.Parameters.AddWithValue("@Crop", model.crop);
            history.Parameters.AddWithValue("@TransTb", "Pd");
            history.Parameters.AddWithValue("@TransType", "Update");
            history.Parameters.AddWithValue("@TransName", "Reject_Packed");
            history.Parameters.AddWithValue("@ChangeValue", "1," + model.issuedto + "," + model.issuedreason);
            history.Parameters.AddWithValue("@Remark", "Issued,Issuedto,Issuedreason");
            history.Parameters.AddWithValue("@UserID", UserID);
            //update
            comm = new SqlCommand("sp_GreenLeaf_REJECT_Packed", conStecDBMS);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.AddWithValue("@bc", model.bc);
            comm.Parameters.AddWithValue("@IssuedTo", model.issuedto);
            comm.Parameters.AddWithValue("@IssuedReason", model.issuedreason);
            comm.Parameters.AddWithValue("@IssuedUser", UserID);

            return Update(conStecDBMS, history, comm);
        }

        public bool Update_Delete_Pack(pd model)
        {
            SqlCommand history, comm;
            string strcommand;
            conStecDBMS.Open();
            //History & Transaction
            strcommand = "DECLARE @t xml " +
                         "SET @t = (	SELECT	bc , " +
                         "					(SELECT * " +
                         "					 FROM [StecDBMS].[dbo].[pd] E " +
                         "					 WHERE E.[bc] = m.bc " +
                         "					 FOR XML PATH ('details'),TYPE) " +
                         "		    	FROM [StecDBMS].[dbo].[pd] m " +
                         "  			WHERE bc = @bc " +
                         "	    		GROUP BY bc " +
                         "  			FOR XML PATH ('oldLog'),type,ROOT('pd')) " +
                         "INSERT INTO Audit_Log " +
                         "VALUES (  @Crop, " +
                         "		    @TransTb, " +
                         "		    GETDATE(), " +
                         "  		@TransType, " +
                         "          @TransName, " +
                         "          NULL, " +
                         "          NULL, " +
                         "          @Remark, " +
                         "          @UserID, " +
                         "          @t) ";
            history = new SqlCommand(strcommand, conStecDBMS);
            history.Parameters.AddWithValue("@bc", model.bc);
            history.Parameters.AddWithValue("@Crop", model.crop);
            history.Parameters.AddWithValue("@TransTb", "Pd");
            history.Parameters.AddWithValue("@TransType", "Delete");
            history.Parameters.AddWithValue("@TransName", "Delete_Packed");
            history.Parameters.AddWithValue("@Remark", "Delete barcode = " + model.bc);
            history.Parameters.AddWithValue("@UserID", UserID);
            //update
            comm = new SqlCommand("sp_GreenLeaf_DEL_Packed", conStecDBMS);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.AddWithValue("@bc", model.bc);
            comm.Parameters.AddWithValue("@Crop", model.crop);

            return Update(conStecDBMS, history, comm);
        }

        public bool Update_UnlockBlending(sp_QC_SEL_BLENDINGHOUR_Result model, string TransName)
        {
            try
            {
                using (StecDBMSEntities stectDbms = new StecDBMSEntities())
                {
                    stectDbms.sp_QC_UNLOCK_BLENDINGHOUR(model.pdno, model.blendinghour, UserID);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update_UnlockPacking(sp_QC_SEL_PACKINGHOUR_Result model, string TransName)
        {
            try
            {
                using (StecDBMSEntities stectDbms = new StecDBMSEntities())
                {
                    stectDbms.sp_QC_UNLOCK_PACKINGHOUR(model.pdno, model.packinghour, UserID);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update_Mat_By_Excel2(mat model)
        {
            SqlCommand history, comm;
            string strcommand;
            conStecDBMS.Open();
            //History&Transaction
            strcommand = "DECLARE @t xml " +
                         "SET @t = (	SELECT	bc , " +
                         "					    (SELECT [rcno],[crop],[type],[subtype],[company],[rcfrom],[bc],[supplier] " +
                         "					            ,[supplier2],[baleno],[green],[classify],[mark],[weight],[weightbuy],[malcam] " +
                         "								,[hearson],[docno],[frombc],[fromclassify],[frommark],[fromcompany],[tobc],[toclassify] " +
                         "								,[tomark],[tocompany],[picking],[bz],[pallet],[pvpallet],[bay],[wh],[pvwh],[isno] " +
                         "								,[issued],[issuedto],[issueddate],[issuedtime],[issuedreason],[priceunit],[price],[priceno] " +
                         "								,[pvpriceunit],[pvprice],[pvpriceno],[firstprice],[adjpriceno],[adjdate],[adjtime],[adjtimes] " +
                         "								,[paidno],[paid],[rcstatus],[leafstatus],[locked],[dtrecord],[receiveduser],[issueduser] " +
                         "								,[adjuser],[qcuser],[oldbc],[tcharge],[leaflocked],[feedinguser],[blendingprice],[blendingpriceunit] " +
                         "								,[feedingdate],[feedingtime],[topddate],[toprgrade],[topdno],[topdhour],[frompddate],[fromprgrade] " +
                         "								,[frompdno],[frompdhour],[pkvalue],[price2],[price3],[gep%] as gep,[gepbaht],[redrycharge] " +
                         "								,[pdcost],[cropdef],[oldprice],[oldpriceunit],[bfno],[bftimes],[ntrm_no],[ntrm_nylon] " +
                         "								,[ntrm_plastic],[ntrm_rubber],[ntrm_string],[ntrm_metal],[ntrm_other],[feedingweight],[saleweight],[moisture_status] " +
                         "								,[rcdate],[wet],[CPAtestNo],[handle_charge],[price_wo_handle_charge],[priceunit_wo_handle_charge],[farmer_code],[id_card],[buying_date] " +
                         "								,[project_code],[topdremark],[frompdremark],[RemarkChecked],[RemarkCheckedDate],[RemarkCheckedUser],[BatchNo],[CPAResult] " +
                         "								,[CPAResultDate],[CPAResultUser],[Classifier],[ReplaceBales] " +
                         "					    FROM [StecDBMS].[dbo].[mat] E " +
                         "					    WHERE E.bc = m.bc " +
                         "  					FOR XML PATH ('details'),TYPE) " +
                         "			    FROM [StecDBMS].[dbo].[mat] m " +
                         "				WHERE bc = @bc " +
                         "				GROUP BY bc " +
                         "				FOR XML PATH ('oldLog'),type,ROOT('mat')) " +
                         "INSERT INTO Audit_Log " +
                         "VALUES (  @Crop, " +
                         "		    @TransTb, " +
                         "		    GETDATE(), " +
                         "  		@TransType, " +
                         "          @TransName, " +
                         "          (SELECT CONCAT(Issued,',',Issuedto,',',Issuedreason,',',Issueduser) FROM MAT WHERE bc = @bc), " +
                         "          @ChangeValue, " +
                         "          @Remark, " +
                         "          @UserID, " +
                         "          @t) ";
            history = new SqlCommand(strcommand, conStecDBMS);
            history.Parameters.AddWithValue("@bc", model.bc);
            history.Parameters.AddWithValue("@Crop", model.crop);
            history.Parameters.AddWithValue("@TransTb", "Mat");
            history.Parameters.AddWithValue("@TransType", "Update");
            history.Parameters.AddWithValue("@TransName", "Update_Stock");
            history.Parameters.AddWithValue("@ChangeValue", "1," + model.issuedto + ","+model.issuedreason+"," + Username);
            history.Parameters.AddWithValue("@Remark", "Issued,Issuedto,Issuedreason,Issueduser");
            history.Parameters.AddWithValue("@UserID", UserID);
            //update
            comm = new SqlCommand("sp_GreenLeaf_REJECT_GREENBale", conStecDBMS);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.AddWithValue("@CROP", model.crop);
            comm.Parameters.AddWithValue("@BC", model.bc);
            comm.Parameters.AddWithValue("@IssuedTo", model.issuedto);
            comm.Parameters.AddWithValue("@IssuedReason", model.issuedreason);
            comm.Parameters.AddWithValue("@IssuedUser", UserID);

            return Update(conStecDBMS, history, comm);
        }

        public bool Update_Mat_By_Excel(mat model)
        {
            try
            {
                using (StecDBMSEntities stectDbms = new StecDBMSEntities())
                {
                    stectDbms.sp_GreenLeaf_REJECT_GREENBale(model.crop, model.bc, model.issuedto, model.issuedto, Username);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Auditlog_Insert(Audit_Log model)
        {
            try
            {
                using (StecDBMSEntities stectDbms = new StecDBMSEntities())
                {
                    model.UserID = UserID;
                    stectDbms.Audit_Log.Add(model);
                    stectDbms.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Auditlog : " + ex.ToString());
            }
        }

        private bool Update(SqlConnection conn, SqlCommand history, SqlCommand comm)
        {
            SqlTransaction transaction;
            transaction = conn.BeginTransaction();
            history.Transaction = transaction;
            comm.Transaction = transaction;
            try
            {
                history.ExecuteNonQuery();
                comm.ExecuteNonQuery();
                transaction.Commit();
                conn.Close();
                return true;
            }
            catch (Exception)
            {
                transaction.Rollback();
                conn.Close();
                return false;
            }
        }

        public bool Unlock_Price_Entry(string docNo)
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                    stecDbms.sp_GreenLeaf_UNLOCK_PRICE_ENTRY(docNo, UserID);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Unlock_Receiving(string rcNo)
        {
            using (StecDBMSEntities stecDbms = new StecDBMSEntities())
            {
                stecDbms.sp_GreenLeaf_UNLOCK_Receiving(rcNo, UserID);
            }
        }

        public void NPI_AddFarmer(NPI_Farmer model)
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                    if (stecDbms.NPI_Farmer.Where(w => w.FarmerCode == model.FarmerCode).Count() > 0)
                    {
                        throw new ArgumentException("ไม่สามารถบันทึกได้ ข้อมูลซ้ำ");
                    }
                    stecDbms.sp_NPI_AddFarmer(model.FarmerCode, model.Prefix, model.FirstName, model.LastName, UserID);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void NPI_UpdateFarmer(NPI_Farmer model)
        {
            try
            {
                using (StecDBMSEntities stecDbms = new StecDBMSEntities())
                {
                    if (stecDbms.NPI_Farmer.Where(w => w.FarmerCode == model.FarmerCode).Count() != 1)
                    {
                        throw new ArgumentException("ไม่สามารถบันทึกได้ กรุณาตรวจสอบ");
                    }
                    stecDbms.sp_NPI_UpdateFarmer(model.FarmerCode, model.Prefix, model.FirstName, model.LastName, UserID);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
