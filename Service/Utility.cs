﻿using System.Net;
using System.Net.Mail;
using System.Collections.Generic;
using InventoryAdjustment.DAL;
using System;
using System.IO;
using System.Windows.Forms;
using System.Data;

namespace InventoryAdjustment.Service
{
    public class Utility
    {
        DataHelper dtHelper = new DataHelper();
        public void SendEmail(string subject, string From, List<Applications_MailTo> MailTo, string Body)
        {
            SmtpClient mailServer = new SmtpClient();
            MailMessage mailMessage = new MailMessage();

            mailServer.Credentials = new NetworkCredential("it-info@siamtobacco.com", "Up2uy@dmin$tecE!");
            mailServer.Port = 587;
            mailServer.Host = "smtp.emailsrvr.com";
            mailMessage.From = new MailAddress("it-info@siamtobacco.com");
            MailTo.ForEach(f => mailMessage.To.Add(f.ReceiverEmail));

            //foreach (string address in CC) { mailMessage.CC.Add(address); }

            mailMessage.Subject = string.Format(subject);
            mailMessage.Body = Body;
            mailServer.Send(mailMessage);
        }

        public List<string> ReadExcelFile(OpenFileDialog fileDialog)
        {
            dynamic result = new DataTable();
            Stream myStream = null;
            try
            {
                myStream = fileDialog.OpenFile();
                if (myStream != null)
                {
                    Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
                    Microsoft.Office.Interop.Excel.Workbook excelBook = excelApp.Workbooks.Open(fileDialog.FileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                    Microsoft.Office.Interop.Excel.Worksheet excelSheet = (Microsoft.Office.Interop.Excel.Worksheet)excelBook.Worksheets.get_Item(1); ;
                    Microsoft.Office.Interop.Excel.Range excelRange = excelSheet.UsedRange;

                    result = GetExcelData(excelRange);

                    excelBook.Close(0);
                    excelApp.Quit();
                }
                if (myStream == null) myStream.Close();
            }
            catch (Exception)
            {
                throw new Exception("An excel is openning please close.");
                //result = null;
            }
            return result;
        }
        private List<string> GetExcelData(Microsoft.Office.Interop.Excel.Range excelRange)
        {
            //DataTable result = new DataTable();
            List<string> result = new List<string>();
            string strCellData = "";
            double douCellData;
            int rowCnt = 0;
            int colCnt = 0;

            //Generate Column
            //for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
            //{
            //    string strColumn = "";
            //    //strColumn = (string)(excelRange.Cells[1, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
            //    strColumn = "Barcode";
            //    result.Columns.Add(strColumn, typeof(string));
            //}

            //Generate Row
            for (rowCnt = 1; rowCnt <= excelRange.Rows.Count; rowCnt++)
            {
                string strData = "";
                for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                {
                    try
                    {
                        strCellData = (string)(excelRange.Cells[rowCnt, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                        strData += strCellData + "|";
                        result.Add(strCellData);
                    }
                    catch (Exception)
                    {
                        douCellData = (excelRange.Cells[rowCnt, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                        strData += douCellData.ToString() + "|";
                        result.Add(douCellData.ToString());
                    }
                }
                strData = strData.Remove(strData.Length - 1, 1);
                //result.Rows.Add(strData.Split('|'));
            }

            return result;
        }

        public string PageUri(string AppName)
        {
            //return "/InventoryAdjustment;component/Form/Pages/" + AppName + ".xaml";
            //string appName = AppName.IndexOf("_Report") >= 0 ? (@"Reports\" + AppName) : AppName;
            string pageUri = @"Form\Pages\" + AppName + ".xaml";
            return pageUri;
        }
        public string unlockType(string strPrarameter)
        {
            switch (strPrarameter)
            {
                case "RGNO":
                    return "Unlock_Regrade";
                case "HSNO":
                    return "Unlock_Handstrip";
                case "TFNO":
                    return "Unlock_Transfer";
                case "RCNO":
                    return "Unlock_Picking";
                default:
                    return "";
            }
        }

        public List<mat> Match_ExcelData(List<string> listBarcode, string Crop)
        {
            var dtHelper = new DataHelper();
            var result = new List<mat>();
            int crop = Convert.ToInt32(Crop);

            foreach (var bc in listBarcode)
            {
                var model = dtHelper.Get_Mat(bc, crop);
                if (model == null) model = new mat { bc = bc, rcfrom = "Waring!", type = "not found" }; // if data doesn't matched
                result.Add(model);
            }

            //listBarcode.ForEach(Barcode =>
            //{
            //    var model = dtHelper.Get_Mat(Barcode, crop);
            //    if (model == null) model = new mat { bc = Barcode, rcfrom = "Waring!", type = "not found" }; // if data doesn't matched
            //    result.Add(model);
            //});
            return result;
        }
    }
}
