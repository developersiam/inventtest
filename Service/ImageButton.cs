﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using InventoryAdjustment.Models;
using InventoryAdjustment.Form;

namespace InventoryAdjustment.Service
{
    public class ImageButton : Button
    {
        Image _image = null;
        TextBlock _textBlock = null;

        public ImageButton(DAL.Application model)
        {
            Name = model.ApplicationName;
            CommandParameter = model.ApplicationName;
            ToolTip = model.Description;
            Width = 130; Height = 100;
            Background = Brushes.WhiteSmoke;
            BorderBrush = Brushes.WhiteSmoke;
            AddHandler(ClickEvent, new RoutedEventHandler(btnMenu_Click));

            StackPanel panel = new StackPanel();
            panel.Orientation = Orientation.Vertical;

            panel.Margin = new Thickness(0);

            _image = new Image();
            _image.Source = new BitmapImage(new Uri(string.Format(@"/InventoryAdjustment;component/icon/menu/{0}", model.Image), UriKind.RelativeOrAbsolute));
            _image.Width = 60; _image.Height = 60;
            panel.Children.Add(_image);

            _textBlock = new TextBlock();
            //_textBlock.Text = model.ApplicationName.Replace("_", " ");
            //_textBlock.Text = model.Name.Trim();
            _textBlock.TextAlignment = TextAlignment.Justify;
            _textBlock.HorizontalAlignment = HorizontalAlignment.Center;
            string[] name = model.Name.Trim().Split('#');
            for (int i = 0; i < name.Length; i++)
            {
                _textBlock.Text += name[i];
                _textBlock.Text += i != name.Length - 1 ? Environment.NewLine : "";
            }
            //_textBlock.TextWrapping = TextWrapping.Wrap;
            panel.Children.Add(_textBlock);

            Content = panel;
        }
        private void btnMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenNewWindow(((Button)sender).CommandParameter.ToString(), ((Button)sender).ToolTip.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void OpenNewWindow(string AppName, string Description)
        {
            GlobalVariable.Application = new DAL.Application { ApplicationName = AppName, Description = Description};
            if (AppName == "Update_Stock") { Update_Stock _obj = new Update_Stock(); _obj.ShowDialog(); }
            else if (AppName == "History") { History _obj = new History(); _obj.ShowDialog(); }
            else if (AppName == "Report") { MenuWindow _obj = new MenuWindow("Report"); _obj.lblHeader.Content = "Report"; _obj.ShowDialog(); }
            else { MainWindow _obj = new MainWindow(); _obj.ShowDialog(); }
        }
    }
}
