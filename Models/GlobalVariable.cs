﻿using InventoryAdjustment.DAL;
using System.Collections.Generic;
using System.Data;

namespace InventoryAdjustment.Models
{
    public static class GlobalVariable
    {
        static User _user;
        public static User User
        {
            get
            {
                return _user;
            }
            set
            {
                _user = value;
            }
        }

        static Application _Application;
        public static Application Application
        {
            get
            {
                return _Application;
            }
            set
            {
                _Application = value;
            }
        }
    }
}
