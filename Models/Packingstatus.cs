﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryAdjustment.Models
{
    public class Packingstatus
    {
        public int Crop { get; set; }
        public string Pdno { get; set; }
        public DateTime Pddate { get; set; }
        public int Packinghour { get; set; }
        public bool Locked { get; set; }
        public bool Def { get; set; }
    }
}
