﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryAdjustment.Models
{
    public class Pd
    {
        public string BC { get; set; }
        public int? Crop { get; set; }
        public int? Caseno { get; set; }
        public bool Issued { get; set; }
        public string IssuedTo { get; set; }
        public string IssuedUser { get; set; }
        public string IssuedReason { get; set; }
        public string PackingDate { get; set; }
        public string PackingTime { get; set; }
        public string FromPdNo { get; set; }
        public int? FromPdHour { get; set; }
        public string Graders { get; set; }
        
    }
}
